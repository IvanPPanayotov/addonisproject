import { Box, ThemeProvider } from "@mui/material";
import "./App.css";
import Header from "./components/Header/Header";
import { AuthProvider } from "./context/AuthenticationContext";
import CustomTheme from "./Theme/themes";
import { useTheme } from "./context/ThemeContext";
import { ModalProvider } from "./context/Open&CloseModals";
import Routing from "./routing/Routing";
import Footer from "./components/Footer/Footer";
import { UniqueValuesProvider } from "./context/UniqueValuesContext";

function App() {
  const { mode } = useTheme();

  return (
    <ThemeProvider theme={CustomTheme(mode)}>
      <ModalProvider>
        <AuthProvider>
          <UniqueValuesProvider>
            <Box
              sx={{
                bgcolor: `background.default`,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
                minHeight: "100vh",
              }}
            >
              <Header />
              <Routing />

              <Footer />
            </Box>
          </UniqueValuesProvider>
        </AuthProvider>
      </ModalProvider>
    </ThemeProvider>
  );
}

export default App;
