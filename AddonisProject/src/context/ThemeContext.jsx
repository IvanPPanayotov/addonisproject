import { createContext, useContext, useState } from "react";

const ThemeContext = createContext();

/**
 * Provides a theme context to its child components.
 * @param {Object} props - The component props.
 * @param {React.ReactNode} props.children - The child components that will have access to the theme context.
 * @returns {React.ReactNode} - The themed child components.
 */
export const ThemeProvider = ({ children }) => {
  const [mode, setMode] = useState(true);

  /**
   * Toggles the theme mode.
   */
  const toggleMode = () => {
    setMode((prevMode) => !prevMode);
  };

  const value = {
    mode,
    toggleMode,
  };

  return <ThemeContext.Provider value={value}>{children}</ThemeContext.Provider>;
};

export const useTheme = () => {
  return useContext(ThemeContext);
};
