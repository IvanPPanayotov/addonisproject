import React, { useContext, useEffect, useState } from "react";
import db from "../config/fireBase.config";
import { set, ref, get } from "firebase/database";
import { updateEmailData } from "../services/users.services";
import {
  reauthenticateWithCredential,
  createUserWithEmailAndPassword,
  getAuth,
  signInWithEmailAndPassword,
  signOut,
  updateEmail,
  EmailAuthProvider,
  sendEmailVerification,
} from "firebase/auth";

const AuthContext = React.createContext();

export function useAuth() {
  return useContext(AuthContext);
}

export const AuthProvider = ({ children }) => {
  const [currentUser, setCurrentUser] = useState(null);
  const [currentUserData, setCurrentUserData] = useState();
  const [isAdmin, setIsAdmin] = useState(false);
  const [loading, setLoading] = useState(true);
  const auth = getAuth();

  useEffect(() => {
    const unSub = auth.onAuthStateChanged(async (user) => {
      setCurrentUser(user);
      if (user) {
        await userData(user.uid);
      } else {
        setCurrentUserData(null);
      }
      setLoading(false);
    });
    return unSub;
  }, []);

  useEffect(() => {
    if (currentUserData) {
      currentUserData && currentUserData.role === "admin"
        ? setIsAdmin(true)
        : setIsAdmin(false);
    }
  }, [currentUserData]);

  const userData = async (userId) => {
    return await get(ref(db, `users/${userId}`)).then((el) => {
      setCurrentUserData(el.val());
    });
  };

  const changeEmail = async (email, password) => {
    try {
      const user = auth.currentUser;

      if (user) {
        const credentials = EmailAuthProvider.credential(user.email, password);
        await reauthenticateWithCredential(user, credentials);
        await updateEmail(user, email);
        await updateEmailData(currentUser, email);
        console.log("Email updated successfully.");
        await sendEmailVerification(user);
      }
    } catch (err) {
      console.log("Error updating email:", err.message);
    }
  };

  const signUp = async (email, password, userName, phoneNumber) => {
    try {
      const auth = getAuth();
      const userCredential = await createUserWithEmailAndPassword(
        auth,
        email,
        password
      );
      await signOut(auth);
      const user = userCredential.user;

      await sendEmailVerification(user);

      await set(ref(db, `users/${user.uid}`), {
        email: email,
        role: "member",
        isBlocked: false,
        userName: userName,
        phoneNumber: phoneNumber,
        avatarPhoto: `https://api.dicebear.com/6.x/thumbs/svg?seed=${user.uid}`,
        userUID: user.uid,
        userVerified: false,
        verifiedDocuments: {},
        createdAddons: {},
        ratedAddons: {},
      });

      return true;
    } catch (error) {
      console.error("Failed to signUp", error);
      return false;
    }
  };

  const logIn = async (email, password) => {
    try {
      const auth = getAuth();
      const userCredential = await signInWithEmailAndPassword(
        auth,
        email,
        password
      );
      const user = userCredential.user;

      if (!user.emailVerified) {
        alert("Verified your email");
        signOut(auth);
        throw Error();
      }
    } catch (error) {
      console.error("Failed to log in:", error);
      throw new Error("failed");
    }
  };

  const logOut = () => {
    return signOut(auth);
  };

  const value = {
    currentUser,
    isAdmin,
    currentUserData,
    setCurrentUserData,
    signUp,
    logIn,
    logOut,
    changeEmail,
  };
  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  );
};
