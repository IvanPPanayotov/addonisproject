import { createContext, useContext, useState } from "react";
import useNavigation from "../hooks/Navigate";

const ModalContext = createContext();

export function ModalProvider({ children }) {
  const [logInModal, setLogInModal] = useState(false);
  const [signInModal, setIsModalOpen] = useState(false);
  const { handleNavigation } = useNavigation();

  const openLogInModal = () => {
    setLogInModal(true);
  };

  const closeLogInModal = () => {
    setLogInModal(false);
  };

  const openSignInModal = () => {
    setIsModalOpen(true);
  };

  const closeSignInModal = () => {
    setIsModalOpen(false);
    handleNavigation("/");
  };

  const value = {
    logInModal,
    openLogInModal,
    closeLogInModal,
    signInModal,
    openSignInModal,
    closeSignInModal,
  };

  return (
    <ModalContext.Provider value={value}>{children}</ModalContext.Provider>
  );
}

export function useModal() {
  return useContext(ModalContext);
}
