import { createContext, useContext, useState, useEffect } from "react";
import { getAllUsers } from "../services/users.services";
import { getAllAddonsFromDatabase } from "../services/addons.services";

const UniqueValuesContext = createContext();

export function useUniqueValues() {
  return useContext(UniqueValuesContext);
}

export function UniqueValuesProvider({ children }) {
  const [uniqueUsers, setUniqueUsers] = useState(new Set());
  const [uniqueEmails, setUniqueEmails] = useState(new Set());
  const [uniquePhones, setUniquePhones] = useState(new Set());
  const [uniqueAddonName, setUniqueAddonName] = useState(new Set());
  const [uniqueGitHubName, setUniqueGitHubName] = useState(new Set());

  useEffect(() => {
    async function fetchUniqueData() {
      try {
        const users = await getAllUsers();
        const addons = await getAllAddonsFromDatabase();

        const usernames = users.map((user) => user.data.userName);
        setUniqueUsers(new Set(usernames));

        const emails = users.map((user) => user.data.email);
        setUniqueEmails(new Set(emails));

        const phones = users.map((user) => user.data.phoneNumber);
        setUniquePhones(new Set(phones));

        const addonsName = addons.map((addon) => addon.addonName);
        setUniqueAddonName(new Set(addonsName));

        const gitHubFileNames = addons.map((addon) => addon.GitHubName);
        setUniqueGitHubName(new Set(gitHubFileNames));
      } catch (error) {
        console.error("Error fetching unique data:", error);
      }
    }
    fetchUniqueData();
  }, []);

  const values = {
    uniqueUsers,
    uniqueEmails,
    uniquePhones,
    uniqueAddonName,
    uniqueGitHubName,
  };

  return (
    <UniqueValuesContext.Provider value={values}>
      {children}
    </UniqueValuesContext.Provider>
  );
}
