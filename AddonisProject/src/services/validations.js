import { EMAIL_REGEX, PASSWORD_REGEX, gitHubRegex } from "../common/constants";

/**
 * Validates a user's username to ensure it meets length requirements.
 *
 * @function
 * @param {string} userName - The username to be validated.
 * @returns {boolean} `true` if the username meets length requirements, `false` otherwise.
 */
export function validateUserName(userName) {
  return userName.length >= 2 && userName.length <= 20;
}

/**
 * Validates a phone number to ensure it meets length requirements.
 *
 * @function
 * @param {string} phone - The phone number to be validated.
 * @returns {boolean} `true` if the phone number meets length requirements, `false` otherwise.
 */
export function validatePhone(phone) {
  return phone.length >= 13 && phone.length <= 15;
}

/**
 * Validates an email address using a regular expression pattern.
 *
 * @function
 * @param {string} email - The email address to be validated.
 * @returns {boolean} `true` if the email address is valid according to the regular expression pattern, `false` otherwise.
 */
export function validateEmail(email) {
  return EMAIL_REGEX.test(email);
}

/**
 * Validates a password using a regular expression pattern.
 *
 * @function
 * @param {string} password - The password to be validated.
 * @returns {boolean} `true` if the password is valid according to the regular expression pattern, `false` otherwise.
 */
export function validatePassword(password) {
  return PASSWORD_REGEX.test(password);
}

/**
 * Validates an addon name to ensure it meets length requirements.
 *
 * @function
 * @param {string} addonName - The addon name to be validated.
 * @returns {boolean} `true` if the addon name meets length requirements, `false` otherwise.
 */
export function validateAddonName(addonName) {
  return addonName.length >= 3 && addonName.length <= 30;
}

/**
 * Validates an addon description to ensure it meets length requirements.
 *
 * @function
 * @param {string} addonDescription - The addon description to be validated.
 * @returns {boolean} `true` if the addon description meets length requirements, `false` otherwise.
 */
export function validateAddonDescription(addonDescription) {
  return addonDescription.length >= 16 && addonDescription.length <= 180;
}

/**
 * Validates the addon tags to ensure it meets length requirements.
 *
 * @function
 * @param {string} tags - The addon tags to be validated.
 * @returns {boolean} `true` if the addon tags meets length requirements, `false` otherwise.
 */
export function validateTags(tags) {
  return tags.length >= 1;
}

/**
 * Validates the URL to ensure it meets the requirements.
 *
 * @function
 * @param {string} inputString - The URL to be validated.
 * @returns {boolean} `true` if the URL meets the requirements, `false` otherwise.
 */
export function isValidURL(inputString) {
  try {
    const url = new URL(inputString);
    if (gitHubRegex.test(url.href)) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
}
