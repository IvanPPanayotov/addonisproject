import { ref, get, update } from "firebase/database";
import db from "../config/fireBase.config";
import { owner, repository } from "../common/constants";
import { octokit } from "../common/constants";

/**
 * Retrieves a list of all addons from the database.
 *
 * @returns {Promise<Array<Object>>} - A Promise that resolves to an array of addon objects,
 * or an empty array if there are no addons in the database.
 * @throws {Error} - If an error occurs while fetching the addons.
 */
export const getAllAddonsFromDatabase = async () => {
  const addonsReference = ref(db, "addons");
  try {
    const addonsSnapshot = await get(addonsReference);
    const addons = [];
    addonsSnapshot.forEach((addonSnapshot) => {
      const addonData = addonSnapshot.val();
      addons.push(addonData);
    });
    return addons;
  } catch (error) {
    console.error("Error getting addons: ", error);
  }
};

/**
 * Sorts an array of addons based on a specified keyword.
 *
 * @param {string} keyword - The sorting keyword, such as "Name," "Upload Date," "Tags," or "Downloads."
 * @param {Array<Object>} addons - An array of addon objects to be sorted.
 * @returns {Array<Object>} - The sorted array of addon objects.
 */
export const sortBy = async (keyword, addons) => {
  const array = [...addons];

  switch (keyword) {
    case "Name":
      array.sort((a, b) => a.addonName.localeCompare(b.addonName));
      break;
    case "Upload Date":
      array.sort((a, b) => {
        const dateA = new Date(a.date);
        const dateB = new Date(b.date);

        if (isNaN(dateA) || isNaN(dateB)) {
          return 0;
        }

        const dateComparison = dateB - dateA;
        if (dateComparison !== 0) {
          return dateComparison;
        }

        const timeComparison = dateB.getTime() - dateA.getTime();
        return timeComparison;
      });
      break;
    case "Tags":
      console.log(array[0].tags.length);
      array.sort((a, b) => b.tags.length - a.tags.length);
      break;
    case "Downloads":
      array.sort((a, b) => b.downloadCount - a.downloadCount);
      break;
  }
  return array;
};

/**
 * Retrieves an addon from the database by its unique key.
 *
 * @param {string} addonId - The unique key or identifier of the addon to retrieve.
 * @returns {Promise<Object|null>} - A Promise that resolves to the addon object if found,
 * or `null` if the addon with the specified key does not exist in the database.
 * @throws {Error} - If an error occurs while fetching the addon.
 */
export const getAddonByKey = async (addonId) => {
  const addonReference = ref(db, `addons/${addonId}`);
  const addon = await get(addonReference);
  return addon.val();
};

/**
 * Updates the status of an addon in the database to "approved."
 *
 * @param {string} addonId - The unique identifier of the addon whose status will be updated.
 * @returns {Promise<void>} - A Promise that resolves when the status is successfully updated.
 * @throws {Error} - If an error occurs during the status update operation.
 */
export const updateAddonStatus = async (addonId) => {
  const updateStatus = {};
  updateStatus[`/addons/${addonId}/status/`] = "approved";
  await update(ref(db), updateStatus);
};

/**
 * Declines and removes an addon from the database by setting its status to "null."
 *
 * @param {string} addonId - The unique identifier of the addon to be declined and removed.
 * @returns {Promise<void>} - A Promise that resolves when the addon is successfully declined and removed.
 * @throws {Error} - If an error occurs during the declining and removal operation.
 */
export const declineAddonStatus = async (addonId) => {
  const updateStatus = {};
  updateStatus[`/addons/${addonId}`] = null;
  await update(ref(db), updateStatus);
};

/**
 * Selects an addon in the database by setting its "selected" status to `true`.
 *
 * @param {string} addonId - The unique identifier of the addon to be selected.
 * @returns {Promise<void>} - A Promise that resolves when the addon is successfully selected.
 * @throws {Error} - If an error occurs during the selection operation.
 */
export const selectAddon = async (addonId) => {
  const updateStatus = {};
  updateStatus[`/addons/${addonId}/selected`] = true;
  await update(ref(db), updateStatus);
};

/**
 * Increments the download count of a specific addon in the database.
 *
 * @param {string} addonId - The unique identifier of the addon whose download count will be incremented.
 * @returns {Promise<void>} - A Promise that resolves when the download count is successfully updated.
 * @throws {Error} - If an error occurs during the download count update operation.
 */
export const updateDownloadCount = async (addonId) => {
  const addonReference = ref(db, `addons/${addonId}/downloadCount/`);
  const addonDownloadCountSnapshot = await get(addonReference);
  if (addonDownloadCountSnapshot.exists()) {
    let data = await addonDownloadCountSnapshot.val();
    const updateStatus = {};
    updateStatus[`/addons/${addonId}/downloadCount/`] = ++data;
    await update(ref(db), updateStatus);
  }
};

/**
 * Retrieves the ten most recently approved addons from the database.
 *
 * @returns {Promise<Array<Object>>} - A Promise that resolves to an array of the ten most recent approved addon objects.
 * @throws {Error} - If an error occurs during the retrieval and sorting operation.
 */
export const getNewestAddons = async () => {
  const addons = await getAllAddonsFromDatabase();
  const filtered = addons.filter((addon) => addon.status === "approved");
  filtered.sort((a, b) => {
    const dateA = new Date(a.date);
    const dateB = new Date(b.date);
    return dateB - dateA;
  });
  const tenMostRecentAddons = filtered.slice(0, 10);
  return tenMostRecentAddons;
};

/**
 * Retrieves the ten most recently approved and featured addons from the database.
 *
 * @returns {Promise<Array<Object>>} - A Promise that resolves to an array of the ten most recent approved and featured addon objects.
 * @throws {Error} - If an error occurs during the retrieval and sorting operation.
 */
export const getFeaturedAddons = async () => {
  const addons = await getAllAddonsFromDatabase();
  const filtered = addons.filter(
    (addon) => addon.selected === true && addon.status === "approved"
  );
  filtered.sort((a, b) => {
    const dateA = new Date(a.date);
    const dateB = new Date(b.date);
    return dateB - dateA;
  });
  const tenMostRecentAddons = filtered.slice(0, 10);
  return tenMostRecentAddons;
};

/**
 * Retrieves the ten most downloaded addons from the database.
 *
 * @async
 * @function
 * @returns {Promise<Array<Object>>} An array of objects representing the ten most downloaded addons.
 * Each object includes addon information such as name, status, and downloadCount.
 * @throws {Error} If there's an issue with fetching the addons from the database.
 */
export const getMostDownloadedAddons = async () => {
  const addons = await getAllAddonsFromDatabase();
  const filtered = addons.filter((addon) => addon.status === "approved");
  filtered.sort((a, b) => {
    const downloadA = a.downloadCount;
    const downloadB = b.downloadCount;
    return downloadB - downloadA;
  });
  const tenMostDownloadedAddons = filtered.slice(0, 10);
  return tenMostDownloadedAddons;
};

/**
 * Retrieves the download count for a specific addon identified by its ID.
 *
 * @async
 * @function
 * @param {string} addonID - The unique identifier for the addon.
 * @returns {Promise<number>} A Promise that resolves to the download count of the specified addon.
 * If the addon does not exist, it returns 0.
 * @throws {Error} If there's an issue with fetching the download count from the database.
 */
export const getAddonDownloadCount = async (addonID) => {
  const addonReference = ref(db, `addons/${addonID}`);
  const addonSnapshot = await get(addonReference);
  if (!addonSnapshot.exists()) {
    return 0;
  }
  const downloadCount = await addonSnapshot.val();
  return downloadCount.downloadCount;
};

/**
 * Deletes a file from a GitHub repository.
 *
 * @async
 * @function
 * @param {string} name - The name of the file to be deleted.
 * @param {string} sha - The SHA (Secure Hash Algorithm) of the file to be deleted.
 * @param {string} email - The email associated with the committer for the deletion.
 * @returns {Promise<void>} A Promise that resolves when the file has been successfully deleted.
 * If `sha` is falsy (e.g., `null` or `undefined`), the function does nothing and resolves immediately.
 * @throws {Error} If there's an issue with deleting the file from the GitHub repository.
 */
export const deleteFromGithub = async (name, sha, email) => {
  if (!sha) {
    return;
  }
  await octokit.request(
    `DELETE /repos/${owner}/${repository}/contents/${name}`,
    {
      owner: owner,
      repo: repository,
      path: name,
      message: `Deleted ${name}`,
      committer: {
        name: owner,
        email: email,
      },
      sha: sha,
      headers: {
        "X-GitHub-Api-Version": "2022-11-28",
      },
    }
  );
};

/**
 * Filters an array of posts to retrieve all posts associated with a specific Integrated Development Environment (IDE).
 *
 * @param {Array<Object>} allPosts - An array of post objects containing information about various posts.
 * @param {string} IDE - The name of the Integrated Development Environment (IDE) to filter by.
 * @returns {Array<Object>} An array of post objects that are associated with the specified IDE.
 */
export const getAllPostsWithIDE = (allPosts, IDE) => {
  const addonsWithFilteredIDE = allPosts.filter(
    (addon) => addon.IDEName === IDE
  );
  return addonsWithFilteredIDE;
};
