import { get, ref, update } from "firebase/database";
import db from "../config/fireBase.config";

/**
 * Like or unlike a post by a user.
 * @param {string} userId - The ID of the user performing the like/unlike action.
 * @param {string} addonID - The ID of the post to be liked/unliked.
 * @returns {Promise<void>} - Resolves after updating the likes information.
 */
export const rateAddon = async (userId, addonID, rate) => {
  const addonReference = ref(db, `/addons/${addonID}`);
  const snapShot = await get(addonReference);

  if (snapShot.exists()) {
    const updateRating = {};
    updateRating[`/addons/${addonID}/ratedBy/${userId}`] = rate;
    updateRating[`/users/${userId}/ratedPosts/${addonID}`] = rate;

    await update(ref(db), updateRating);
  }
};

/**
 * Calculates the average rating for a specific addon based on user ratings.
 *
 * @async
 * @function
 * @param {string} addonID - The unique identifier of the addon for which the average rating is calculated.
 * @returns {Promise<number>} A Promise that resolves to the average rating of the specified addon.
 * If no ratings are available for the addon, it returns 0.
 * @throws {Error} If there's an issue with fetching and calculating ratings from the database.
 */
export const averageRating = async (addonID) => {
  const addonReference = ref(db, `/addons/${addonID}/ratedBy`);
  const snapShot = await get(addonReference);
  if (!snapShot.exists()) {
    return 0;
  }
  const arrayOfRatings = Object.values(await snapShot.val());
  const sum = arrayOfRatings.reduce(
    (acc, currentValue) => acc + currentValue,
    0
  );
  const average = sum / arrayOfRatings.length;
  return average;
};
