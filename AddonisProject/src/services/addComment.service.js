import { ref, push, set } from "firebase/database";
import db from "../config/fireBase.config";

/**
 * Adds a new comment to a specific addon.
 *
 * @param {string} addonID - The ID of the addon to which the comment will be added.
 * @param {string} content - The content of the comment.
 * @param {string} email - The email address of the comment's author.
 * @param {string} avatar - The URL of the author's avatar.
 * @returns {Promise<void>} - A Promise that resolves when the comment is successfully added.
 * @throws {Error} - If the content parameter is empty.
 */
export const addComment = async (addonID, content, email, avatar) => {
  if (!content) {
    return alert(`You need to add a comment`);
  }

  const commentData = {
    comment: content,
    author: email,
    authorAvatar: avatar,
    createdOn: new Date().toLocaleDateString(),
  };

  const newCommentRef = push(
    ref(db, `addons/${addonID}/comments`),
    commentData
  );

  const commentKey = newCommentRef.key;

  commentData.commentID = commentKey;
  await set(ref(db, `addons/${addonID}/comments/${commentKey}`), commentData);
};
