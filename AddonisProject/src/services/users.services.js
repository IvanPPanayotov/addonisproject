import { ref, get, set, update } from "firebase/database";
import db from "../config/fireBase.config";
import { v4 as uuidv4 } from "uuid";
import { owner, repository } from "../common/constants";
import { octokit } from "../common/constants";

/**
 * Retrieves a list of all users from the database.
 *
 * @async
 * @function
 * @returns {Promise<Array<Object>>} A Promise that resolves to an array of user objects.
 * Each user object includes an 'id' representing the unique identifier and 'data' containing user-specific data.
 * @throws {Error} If there's an issue with fetching user data from the database.
 */
export const getAllUsers = async () => {
  /**
   * @typedef {Object} User
   * @property {string} id - The unique identifier of the user.
   * @property {Object} data - The user-specific data retrieved from the database.
   */

  const usersReference = ref(db, "users");
  try {
    const usersSnapshot = await get(usersReference);
    const users = [];
    usersSnapshot.forEach((userSnapshot) => {
      const usersData = userSnapshot.val();
      users.push({
        id: userSnapshot.key,
        data: usersData,
      });
    });

    return users;
  } catch (error) {
    console.error("Error getting users: ", error);
  }
};

/**
 * Retrieves a list of usernames for all authors (users) from the database.
 *
 * @async
 * @function
 * @returns {Promise<Array<string>>} A Promise that resolves to an array of usernames for all authors.
 * @throws {Error} If there's an issue with fetching user data from the database.
 */
export const getAllAuthors = async () => {
  const addonsReference = ref(db, "users");
  const array = [];
  try {
    const usersSnap = await get(addonsReference);
    usersSnap.forEach((user) => {
      const userData = user.val();

      array.push(userData.userName);
    });

    return array;
  } catch (error) {
    console.error("Error getting users: ", error);
  }
};

/**
 * Retrieves an array of addon IDs associated with a specific user (author) by their username.
 *
 * @async
 * @function
 * @param {string} userName - The username of the user (author) for whom addon IDs are to be retrieved.
 * @returns {Promise<Array<string>>} A Promise that resolves to an array of addon IDs associated with the specified user.
 * If the user does not exist or has no addons, it returns an empty array.
 * @throws {Error} If there's an issue with fetching user data from the database.
 */
export const getAddonsFromUserName = async (userName) => {
  /**
   * @typedef {Object} User
   * @property {string} userName - The username of the user.
   * @property {Array<Object>} addons - An array of addon objects associated with the user.
   */

  const addonsReference = ref(db, "users");
  const array = [];
  try {
    const usersSnap = await get(addonsReference);
    usersSnap.forEach((user) => {
      const userData = user.val();
      if (userData.userName === userName) {
        if (!userData.addons) {
          return;
        }
        userData.addons.forEach((addon) => {
          array.push(addon.addonId);
          return;
        });
      }
    });

    return array;
  } catch (error) {
    console.error("Error getting users: ", error);
  }
};

/**
 * Retrieves an array of addon objects with a "pending" status from the database.
 *
 * @async
 * @function
 * @returns {Promise<Array<Object>>} A Promise that resolves to an array of addon objects with a "pending" status.
 * If no addons with "pending" status are found, it returns an empty array.
 * @throws {Error} If there's an issue with fetching addon data from the database.
 */
export const getAllPendingFromDatabase = async () => {
  /**
   * @typedef {Object} Addon
   * @property {string} status - The status of the addon.
   * @property {string} // Add other properties of the addon as needed.
   */

  const addonsReference = ref(db, "addons");
  const array = [];
  try {
    const addonsSnapshot = await get(addonsReference);
    addonsSnapshot.forEach((addonSnapshot) => {
      const addonData = addonSnapshot.val();
      if (addonData.status === "pending") {
        array.push(addonData);
      }
    });

    return array;
  } catch (error) {
    console.error("Error getting addons: ", error);
  }
};

/**
 * Retrieves an array of addon objects with an "approved" status from the database.
 *
 * @async
 * @function
 * @returns {Promise<Array<Object>>} A Promise that resolves to an array of addon objects with an "approved" status.
 * If no addons with "approved" status are found, it returns an empty array.
 * @throws {Error} If there's an issue with fetching addon data from the database.
 */
export const getAllApprovedFromDatabase = async () => {
  /**
   * @typedef {Object} Addon
   * @property {string} status - The status of the addon.
   * @property {string} // Add other properties of the addon as needed.
   */

  const addonsReference = ref(db, "addons");
  const array = [];
  try {
    const addonsSnapshot = await get(addonsReference);
    addonsSnapshot.forEach((addonSnapshot) => {
      const addonData = addonSnapshot.val();
      if (addonData.status === "approved") {
        array.push(addonData);
      }
    });

    return array;
  } catch (error) {
    console.error("Error getting addons: ", error);
  }
};

/**
 * Updates the blocking status of a user in the database.
 *
 * @async
 * @function
 * @param {Object} user - The user object containing user information.
 * @param {boolean} status - The blocking status to set for the user. `true` to block, `false` to unblock.
 * @returns {Promise<void>} A Promise that resolves when the user's blocking status has been updated in the database.
 * @throws {Error} If there's an issue with updating the user's blocking status.
 */
export const blockUser = async (user, status) => {
  const updates = {
    [`/users/${user.userUID}/isBlocked`]: status,
  };
  await update(ref(db), updates);
};

/**
 * Marks a user as identified or verified in the database.
 *
 * @async
 * @function
 * @param {string} userId - The unique identifier of the user to be marked as identified.
 * @returns {Promise<void>} A Promise that resolves when the user's identification status has been updated in the database.
 * @throws {Error} If there's an issue with updating the user's identification status.
 */
export const makeIdentifyUser = async (userId) => {
  const updates = {
    [`/users/${userId}/userVerified`]: true,
  };
  await update(ref(db), updates);
};

/**
 * Declines or clears a user's verified documents in the database.
 *
 * @async
 * @function
 * @param {string} userId - The unique identifier of the user for whom verified documents are to be declined or cleared.
 * @returns {Promise<void>} A Promise that resolves when the user's verified documents have been declined or cleared in the database.
 * @throws {Error} If there's an issue with declining or clearing the user's verified documents.
 */
export const declineUserDocuments = async (userId) => {
  const updates = {
    [`/users/${userId}/verifiedDocuments`]: null,
  };
  await update(ref(db), updates);
};

/**
 * Grants or updates the administrative role of a user in the database.
 *
 * @async
 * @function
 * @param {Object} userId - The user object containing user information, including the unique identifier.
 * @param {string} role - The administrative role to assign to the user.
 * @returns {Promise<void>} A Promise that resolves when the user's administrative role has been updated in the database.
 * @throws {Error} If there's an issue with granting or updating the user's administrative role.
 */
export const makeAdmin = async (userId, role) => {
  const updates = {
    [`/users/${userId.userUID}/role`]: role,
  };
  await update(ref(db), updates);
};

/**
 * Updates the email address associated with a user in the database.
 *
 * @async
 * @function
 * @param {string} userId - The unique identifier of the user whose email address is to be updated.
 * @param {string} email - The new email address to assign to the user.
 * @returns {Promise<void>} A Promise that resolves when the user's email address has been updated in the database.
 * @throws {Error} If there's an issue with updating the user's email address.
 */
export const updateEmailData = async (userId, email) => {
  const updates = {
    [`/users/${userId.uid}/email`]: email,
  };
  await update(ref(db), updates);
};

/**
 * Updates the phone number associated with a user in the database.
 *
 * @async
 * @function
 * @param {string} userId - The unique identifier of the user whose phone number is to be updated.
 * @param {string} phone - The new phone number to assign to the user.
 * @returns {Promise<void>} A Promise that resolves when the user's phone number has been updated in the database.
 * @throws {Error} If there's an issue with updating the user's phone number.
 */
export const updatePhoneData = async (userId, phone) => {
  const updates = {
    [`/users/${userId.uid}/phoneNumber/`]: phone,
  };
  await update(ref(db), updates);
};

/**
 * Uploads a user's profile picture data to the database.
 *
 * @async
 * @function
 * @param {string} userId - The unique identifier of the user for whom the profile picture data is to be uploaded.
 * @param {string} picture - The URL or data representing the user's profile picture.
 * @returns {Promise<void>} A Promise that resolves when the user's profile picture data has been uploaded to the database.
 * @throws {Error} If there's an issue with uploading the user's profile picture data.
 */
export const uploadProfilePictureData = async (userId, picture) => {
  const updates = {
    [`/users/${userId}/verifiedDocuments/profilePicture`]: picture,
  };
  await update(ref(db), updates);
};

/**
 * Uploads a user's document picture data to the database.
 *
 * @async
 * @function
 * @param {string} userId - The unique identifier of the user for whom the document picture data is to be uploaded.
 * @param {string} picture - The URL or data representing the user's document picture.
 * @returns {Promise<void>} A Promise that resolves when the user's document picture data has been uploaded to the database.
 * @throws {Error} If there's an issue with uploading the user's document picture data.
 */
export const uploadDocumentPictureData = async (userId, picture) => {
  const updates = {
    [`/users/${userId}/verifiedDocuments/documentPicture`]: picture,
  };
  await update(ref(db), updates);
};

/**
 * Retrieves a list of all addons from a specified GitHub repository.
 *
 * @async
 * @function
 * @returns {Promise<Array<Object>>} A Promise that resolves to an array of addon objects retrieved from the GitHub repository.
 * @throws {Error} If there's an issue with fetching addon data from the repository.
 */
export const getAllAddons = async () => {
  const addons = await octokit.request(
    `GET /repos/${owner}/${repository}/contents`,
    {
      owner: owner,
      repo: repository,
    }
  );
  return addons.data;
};

/**
 * Retrieves addon information, including its download URL, from a specified GitHub repository.
 *
 * @async
 * @function
 * @param {string} name - The name of the addon for which information is to be retrieved.
 * @returns {Promise<string>} A Promise that resolves to the download URL of the specified addon.
 * @throws {Error} If there's an issue with fetching addon information from the repository.
 */
export const getAddonInformation = async (name) => {
  const addons = await octokit.request(
    `GET /repos/${owner}/${repository}/contents/${name}`,
    {
      owner: owner,
      repo: repository,
    }
  );
  return addons.data.download_url;
};

/**
 * Retrieves detailed information about an addon from a specified GitHub repository.
 *
 * @async
 * @function
 * @param {string} name - The name of the addon for which detailed information is to be retrieved.
 * @returns {Promise<Object>} A Promise that resolves to an object containing detailed information about the specified addon.
 * @throws {Error} If there's an issue with fetching addon information from the repository.
 */
export const getAddonInformationAll = async (name) => {
  const addons = await octokit.request(
    `GET /repos/${owner}/${repository}/contents/${name}`,
    {
      owner: owner,
      repo: repository,
    }
  );
  return addons.data;
};

/**
 * Uploads a file to a specified location in a GitHub repository.
 *
 * @async
 * @function
 * @param {File} uploadFile - The file to be uploaded.
 * @returns {Promise<void>} A Promise that resolves when the file has been successfully uploaded to the repository.
 * @throws {Error} If there's an issue with uploading the file to the repository.
 */
export const uploadFile = async (uploadFile) => {
  const addonName = uploadFile.name;
  const fileContent = await readFileContent(uploadFile);

  const encodedContent = arrayBufferToBase64(fileContent);

  const createFileData = {
    owner: owner,
    repo: repository,
    path: addonName,
    message: `Upload ${addonName}`,
    content: encodedContent,
  };

  try {
    const response = await octokit.request(
      `PUT /repos/${owner}/${repository}/contents/${addonName}`,
      createFileData
    );
  } catch (error) {
    console.error("Error uploading file:", error);
  }
};

/**
 * Reads the content of a File object and resolves with the content as an ArrayBuffer.
 *
 * @function
 * @param {File} file - The File object from which to read the content.
 * @returns {Promise<ArrayBuffer>} A Promise that resolves with the content of the File object as an ArrayBuffer.
 * @throws {Error} If there's an issue with reading the file content.
 */
function readFileContent(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = (event) => {
      resolve(event.target.result);
    };

    reader.onerror = (event) => {
      reject(event.error);
    };

    reader.readAsArrayBuffer(file);
  });
}

/**
 * Converts an ArrayBuffer to a Base64-encoded string.
 *
 * @function
 * @param {ArrayBuffer} arrayBuffer - The ArrayBuffer to be converted to Base64.
 * @returns {string} A Base64-encoded string representing the contents of the ArrayBuffer.
 */
function arrayBufferToBase64(arrayBuffer) {
  const binary = new Uint8Array(arrayBuffer);
  const bytes = [];

  for (const byte of binary) {
    bytes.push(String.fromCharCode(byte));
  }

  return btoa(bytes.join(""));
}

/**
 * Retrieves a user document from the database by their unique identifier.
 *
 * @async
 * @function
 * @param {string} userId - The unique identifier of the user whose document is to be retrieved.
 * @returns {Promise<Object|null>} A Promise that resolves with the user document object if found, or `null` if the user does not exist.
 * @throws {Error} If there's an issue with retrieving the user document from the database.
 */
export const getUserDocument = async (userId) => {
  const userRef = ref(db, `users/${userId}`);
  return get(userRef)
    .then((userSnap) => {
      if (userSnap.exists()) {
        return userSnap.val();
      } else {
        return null;
      }
    })
    .catch((error) => {
      console.error("Error retrieving user document:", error);
      throw error;
    });
};

/**
 * Uploads an addon to the database and associates it with the user.
 *
 * @async
 * @function
 * @param {string} userUid - The unique identifier of the user uploading the addon.
 * @param {string} userAvatar - The avatar of the user uploading the addon.
 * @param {string} userEmail - The email of the user uploading the addon.
 * @param {string} uploadedPicture - The picture uploaded for the addon.
 * @param {string} name - The name of the addon.
 * @param {string} description - The description of the addon.
 * @param {string} originLink - The origin link of the addon.
 * @param {string} IDEName - The name of the integrated development environment (IDE) for the addon.
 * @param {string[]} tags - An array of tags associated with the addon.
 * @param {string} addonInformation - Information about the addon.
 * @param {string} SHA - The SHA (Secure Hash Algorithm) value associated with the addon.
 * @param {string} uploadedFileName - The name of the file uploaded for the addon.
 * @returns {Promise<void>} A Promise that resolves when the addon has been successfully uploaded to the database.
 * @throws {Error} If there's an issue with uploading the addon or updating user information.
 */
export const uploadAddonService = async (
  userUid,
  userAvatar,
  userEmail,
  uploadedPicture,
  name,
  description,
  originLink,
  IDEName,
  tags,
  addonInformation,
  SHA,
  uploadedFileName
) => {
  const now = new Date();
  const formattedDate = `${
    now.getMonth() + 1
  }/${now.getDate()}/${now.getFullYear()} ${now.getHours()}:${now.getMinutes()}`;

  const addonId = uuidv4();
  await set(ref(db, `addons/${addonId}`), {
    userID: userUid,
    addonID: addonId,
    avatar: userAvatar,
    author: userEmail,
    addonName: name,
    addonPicture: uploadedPicture,
    description: description,
    tags: tags,
    originLink: originLink,
    IDEName: IDEName,
    comments: ``,
    date: formattedDate,
    likedBy: {},
    downloadCount: 0,
    status: "pending",
    downloadLink: addonInformation,
    selected: false,
    SHA: SHA,
    GitHubName: uploadedFileName,
  });

  const snapshot = await get(ref(db, `addons/${addonId}`));
  if (snapshot.exists()) {
    const addonData = snapshot.val();
    importTags(tags, addonData);
  }

  const userRef = ref(db, `users/${userUid}`);

  getUserDocument(userUid)
    .then((userSnap) => {
      if (userSnap) {
        let userAddons = userSnap.addons || [];
        if (!Array.isArray(userAddons)) {
          userAddons = [];
        }
        userAddons.push({
          userID: userUid,
          addonId: addonId,
          author: userEmail,
          addonName: name,
          description: description,
          date: new Date().toLocaleDateString(),
        });

        update(userRef, { addons: userAddons })
          .then(() => {})
          .catch((error) => {
            alert("Error updating user document:", error);
          });
      }
    })
    .catch((error) => {
      console.error("Error retrieving user document:", error);
    });
};

/**
 * Imports tags associated with an addon into the database.
 *
 * @function
 * @param {string[]} arrayOfTags - An array of tags to be associated with the addon.
 * @param {Object} addonData - An object containing data of the addon to which tags will be associated.
 * @throws {Error} If there's an issue with importing tags into the database.
 */
const importTags = (arrayOfTags, addonData) => {
  arrayOfTags.map((tag) => {
    set(ref(db, `tags/${tag}/${addonData.addonID}`), addonData);
  });
};

/**
 * Changes a user's profile picture URL in the database.
 *
 * @async
 * @function
 * @param {string} userId - The unique identifier of the user whose profile picture URL is to be updated.
 * @param {string} url - The new profile picture URL.
 * @throws {Error} If there's an issue with updating the user's profile picture URL in the database.
 */
export const changePicture = async (userId, url) => {
  const userReference = ref(db, `users/${userId}/`);
  await update(userReference, { avatarPhoto: url });
};

/**
 * Retrieves the date of the last commit for an addon from a specified GitHub repository.
 *
 * @async
 * @function
 * @param {Object[]} addon - An array of addon objects containing information about the addon, including the origin link.
 * @returns {Promise<string|null>} A Promise that resolves with the formatted date of the last commit, or `null` if the last commit date couldn't be retrieved.
 * @throws {Error} If there's an issue with fetching repository information or determining the last commit date.
 */
export const getLastCommit = async (addon) => {
  for (const data of addon) {
    try {
      let owner = `Ivan-P-P99`;
      let repo = `StorageAddonis`;

      if (data.originLink && data.originLink.includes(`https`)) {
        const urlParts = data.originLink.split("/");
        owner = urlParts[3];
        repo = urlParts[4];
      }

      const response = await octokit.request(
        "GET /repos/:owner/:repo/commits",
        {
          owner: owner,
          repo: repo,
        }
      );

      const lastCommitDate = response.data[0].commit.author.date;
      const dateObject = new Date(lastCommitDate);

      const formattedDate = dateObject.toLocaleDateString();
      return formattedDate;
    } catch (error) {
      console.error("Error fetching repository information:", error);
    }
  }
};

/**
 * Retrieves the count of open issues for an addon from a specified GitHub repository.
 *
 * @async
 * @function
 * @param {Object[]} addon - An array of addon objects containing information about the addon, including the origin link.
 * @returns {Promise<number|null>} A Promise that resolves with the count of open issues, or `null` if the count couldn't be retrieved.
 * @throws {Error} If there's an issue with fetching repository information or determining the count of open issues.
 */
export const getIssues = async (addon) => {
  for (const data of addon) {
    try {
      let owner = `Ivan-P-P99`;
      let repo = `StorageAddonis`;

      if (data.originLink && data.originLink.includes(`https`)) {
        const urlParts = data.originLink.split("/");
        owner = urlParts[3];
        repo = urlParts[4];
      }

      const response = await octokit.request("GET /repos/:owner/:repo", {
        owner: owner,
        repo: repo,
      });

      const openIssuesCount = response.data.open_issues_count;

      return openIssuesCount;
    } catch (error) {
      console.error("Error fetching repository information:", error);
    }
  }
};

/**
 * Retrieves the count of open pull requests for an addon from a specified GitHub repository.
 *
 * @async
 * @function
 * @param {Object[]} addon - An array of addon objects containing information about the addon, including the origin link.
 * @returns {Promise<number|null>} A Promise that resolves with the count of open pull requests, or `null` if the count couldn't be retrieved.
 * @throws {Error} If there's an issue with fetching repository information or determining the count of open pull requests.
 */
export const getPullRequests = async (addon) => {
  for (const data of addon) {
    try {
      let owner;
      let repo;

      if (data.originLink && data.originLink.includes(`https`)) {
        const urlParts = data.originLink.split("/");
        owner = urlParts[3];
        repo = urlParts[4];
      } else {
        owner = `Ivan-P-P99`;
        repo = `StorageAddonis`;
      }

      const response = await octokit.request("GET /repos/:owner/:repo/pulls", {
        owner: owner,
        repo: repo,
      });
      const openPullRequestsCount = response.data.length;
      return openPullRequestsCount;
    } catch (error) {
      console.error("Error fetching repository information:", error);
    }
  }
};

/**
 * Retrieves the download link for an addon from a specified GitHub repository or returns the download link directly if it's already provided.
 *
 * @async
 * @function
 * @param {Object} addon - An object containing information about the addon, including the download link and origin link.
 * @returns {Promise<string|null>} A Promise that resolves with the download link for the addon, or `null` if the download link couldn't be retrieved.
 * @throws {Error} If there's an issue with fetching repository information or determining the download link.
 */
export const downloadAddon = async (addon) => {
  try {
    let owner;
    let repo;

    if (addon.downloadLink && !addon.downloadLink.includes(`Ivan-P-P99`)) {
      const urlParts = addon.originLink.split("/");
      owner = urlParts[3];
      repo = urlParts[4];
    } else {
      return addon.downloadLink;
    }

    const response = await octokit.request("GET /repos/:owner/:repo", {
      owner: owner,
      repo: repo,
    });

    const branch = response.data.default_branch;
    return `https://github.com/${owner}/${repo}/archive/refs/heads/${branch}.zip`;
  } catch (error) {
    console.error("Error fetching repository information:", error);
  }
};
