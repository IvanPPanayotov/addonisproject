import { get, ref } from "firebase/database";
import db from "../config/fireBase.config";

/**
 * Filters addons or users based on search criteria and selected filters.
 *
 * @async
 * @function
 * @param {Array<Object>|null} addons - An array of addon objects or null if the filter is not for addons.
 * @param {string} searchText - The search text to match against addon or user properties.
 * @param {string} selectedFilter - The selected filter for searching, which can be "IDE," "Name," "Username," or "Email."
 * @returns {Promise<Array<Object>>} A Promise that resolves to an array of filtered addon objects or user objects,
 * based on the selected filter and search criteria. If no matches are found, it returns an empty array.
 * @throws {Error} If there's an issue with fetching addon or user data from the database.
 */
export const onSearch = async (addons, searchText, selectedFilter) => {
  /**
   * @typedef {Object} Addon
   * @property {string} addonName - The name of the addon.
   * @property {string} IDEName - The name of the Integrated Development Environment (IDE) associated with the addon.
   */

  /**
   * @typedef {Object} User
   * @property {string} userName - The username of the user.
   * @property {string} email - The email address of the user.
   */
  if (selectedFilter === "IDE" || selectedFilter === "Name") {
    const filteredAddons = addons.filter((addon) => {
      const searchTextLower = searchText.toLowerCase();

      if (selectedFilter === "Name") {
        const addonNameLower = addon.addonName.toLowerCase();
        return addonNameLower.startsWith(searchTextLower);
      } else if (selectedFilter === "IDE") {
        const addonIDELower = addon.IDEName.toLowerCase();
        return addonIDELower.startsWith(searchTextLower);
      }
    });
    return filteredAddons;
  } else if (selectedFilter === "Username" || selectedFilter === "Email") {
    const usersRef = ref(db, "users/");
    const usersSnapshot = await get(usersRef);

    if (usersSnapshot.exists()) {
      const allUsersArray = usersSnapshot.val();
      const filteredUsers = Object.values(allUsersArray).filter((user) => {
        const searchTextLower = searchText.toLowerCase();

        if (selectedFilter === "Username") {
          const userNameLower = user.userName.toLowerCase();

          return userNameLower.startsWith(searchTextLower);
        } else if (selectedFilter === "Email") {
          const userEmailLower = user.email.toLowerCase();
          return userEmailLower.startsWith(searchTextLower);
        }

        return false;
      });
      return filteredUsers;
    }
  }
};
