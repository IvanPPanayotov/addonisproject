import { get, ref, update } from "firebase/database";
import db from "../config/fireBase.config";

/**
 * Deletes a specific addon associated with a user from the database.
 *
 * @async
 * @function
 * @param {string} userID - The unique identifier of the user whose addon is to be deleted.
 * @param {string} addonID - The unique identifier of the addon to be deleted.
 * @returns {Promise<void>} A Promise that resolves when the addon has been successfully deleted.
 * If the user or addon does not exist, or if there are no addons, the function does nothing and resolves immediately.
 * @throws {Error} If there's an issue with deleting the addon from the user's data in the database.
 */
export const deleteFromUser = async (userID, addonID) => {
  const snapshot = await get(ref(db, `users/${userID}`));

  if (snapshot.exists()) {
    const postData = snapshot.val();
    if (!postData.addons) {
      return;
    }

    Object.values(postData.addons).map(async (addon, index) => {
      if (addon.addonId === addonID) {
        const updates = {
          [`/users/${userID}/addons/${index}`]: null,
        };
        await update(ref(db), updates);
      }
    });
  }
};

/**
 * Deletes tags associated with a specific addon and removes the addon from user data.
 *
 * @async
 * @function
 * @param {string} addonID - The unique identifier of the addon for which tags should be deleted.
 * @returns {Promise<void>} A Promise that resolves when the tags have been successfully deleted
 * and the addon has been removed from user data. If the addon or its tags do not exist, or if there
 * are no tags associated with the addon, the function does nothing and resolves immediately.
 * @throws {Error} If there's an issue with deleting the tags or removing the addon from user data in the database.
 */
const deleteTags = async (addonID) => {
  const snapshot = await get(ref(db, `addons/${addonID}`));

  if (snapshot.exists()) {
    const postData = snapshot.val();
    if (!postData.tags) {
      return;
    }
    postData.tags.map(async (tag) => {
      const updates = {
        [`/tags/${tag}/${addonID}`]: null,
      };
      await update(ref(db), updates);
    });
    await deleteFromUser(postData.userID, postData.addonID);
  }
};

/**
 * Deletes data from a specified folder in the database and optionally triggers related actions.
 *
 * @async
 * @function
 * @param {string} folder - The name of the folder in the database from which data should be deleted.
 * @param {string} key - The primary key identifying the data to be deleted.
 * @param {string|null} secondaryKey - The optional secondary key, if applicable.
 * @param {string|null} thirdKey - The optional third key, if applicable.
 * @returns {Promise<void>} A Promise that resolves when the data has been successfully deleted
 * from the specified folder. If the folder, key, or secondaryKey does not exist, the function does nothing
 * and resolves immediately.
 * @throws {Error} If there's an issue with deleting the data from the database or related actions.
 */
export const deleteFunc = async (folder, key, secondaryKey, thirdKey) => {
  let updates = null;
  if (thirdKey) {
    updates = {
      [`/${folder}/${key}/${secondaryKey}/${thirdKey}`]: null,
    };
  } else if (secondaryKey) {
    updates = {
      [`/${folder}/${key}/${secondaryKey}`]: null,
    };
  } else {
    updates = {
      [`/${folder}/${key}`]: null,
    };
  }
  if (folder == `addons`) {
    await deleteTags(key);
  }
  await update(ref(db), updates);
};
