import { get, ref, update } from "firebase/database";
import db from "../config/fireBase.config";
import { getAllAddonsFromDatabase } from "./addons.services";

/**
 * Get an array of all available tags.
 * @returns {Promise<Array>} - The array of tags.
 */
export const getAllTags = async () => {
  const postsReference = ref(db, "tags");
  try {
    const tagsSnapshot = await get(postsReference);
    if (!tagsSnapshot.exists()) {
      return [];
    }
    return Object.keys(tagsSnapshot.val());
  } catch (error) {
    console.error("Error getting posts: ", error);
  }
};

/**
 * Get an array of all posts associated with a specific tag.
 * @param {string} tag - The tag to filter the posts by.
 * @returns {Promise<Array>} - The array of posts with the specified tag.
 */
export const getAllPostsFromTag = async (tag) => {
  if (!tag) {
    return await getAllAddonsFromDatabase();
  }
  const tagReference = ref(db, `tags/${tag}`);
  try {
    const tagsSnapshot = await get(tagReference);
    if (!tagsSnapshot.exists()) {
      return [];
    }

    const arrayOfPosts = [];
    tagsSnapshot.forEach((postTagSnapshot) => {
      const postDetails = postTagSnapshot.val();
      arrayOfPosts.push(postDetails);
    });
    return arrayOfPosts;
  } catch (error) {
    console.error("Error getting posts: ", error);
  }
};

/**
 * Delete tags associated with a post.
 * @param {string} postId - The ID of the post for which tags will be deleted.
 * @returns {Promise<void>} - Resolves after deleting tags.
 */
const deleteTags = async (postId) => {
  // todo connect to delete tags
  const snapshot = await get(ref(db, `posts/${postId}`));

  if (snapshot.exists()) {
    const postData = snapshot.val();
    if (!postData.tags) {
      return;
    }
    postData.tags.map(async (tag) => {
      const updates = {
        [`/tags/${tag}/${postId}`]: null,
      };
      await update(ref(db), updates);
    });
  }
};
