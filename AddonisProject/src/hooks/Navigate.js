import { useNavigate } from "react-router-dom";

const useNavigation = () => {
  const navigate = useNavigate();

  const handleNavigation = (path, state) => {
    navigate(path, { state });
  };

  return { handleNavigation };
};

export default useNavigation;
