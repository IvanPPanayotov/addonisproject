import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Tooltip from "@mui/material/Tooltip";
import Settings from "@mui/icons-material/Settings";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import { useAuth } from "../../context/AuthenticationContext";
import useNavigation from "../../hooks/Navigate";
import { useState } from "react";

export default function UserProfileCard() {
  const [anchorEl, setAnchorEl] = useState(null);
  const [success, setSuccess] = useState(false);

  const open = Boolean(anchorEl);
  const { logOut, currentUserData } = useAuth();
  const { handleNavigation } = useNavigation();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  if (!currentUserData) {
    return null;
  }

  const handleSettings = () => {
    handleNavigation("/settings");
    handleClose();
  };

  const handleLogOut = () => {
    logOut();
    setSuccess(true);
    handleClose();
    handleNavigation("/");
  };

  return (
    <>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          textAlign: "center",
          marginLeft: "auto",
        }}
      >
        <Tooltip title="Profile Details">
          <IconButton
            onClick={handleClick}
            size="small"
            sx={{ ml: 2 }}
            aria-controls={open ? "account-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
          >
            <Avatar
              sx={{ width: 60, height: 60, backgroundColor: "white" }}
              src={currentUserData.avatarPhoto}
            ></Avatar>
          </IconButton>
        </Tooltip>
      </Box>
      <Menu
        sx={{
          display: "flex",
          alignItems: "center",
          textAlign: "center",

          "& .MuiAvatar-root": {
            width: 80,
            height: 80,
          },
        }}
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        <Avatar
          src={currentUserData.avatarPhoto}
          sx={{ width: 32, height: 32, m: "auto" }}
        />

        <Typography variant="h6" gutterBottom>
          {currentUserData.userName}
        </Typography>
        <Typography variant="body2" color="textSecondary">
          {currentUserData.email}
        </Typography>
        <Typography variant="body2" color="textSecondary">
          Role: {currentUserData.role}
        </Typography>

        <Divider />

        <MenuItem onClick={handleSettings}>
          <ListItemIcon>
            <Settings fontSize="small" />
          </ListItemIcon>
          Settings
        </MenuItem>

        <MenuItem
          onClick={() => {
            handleLogOut();
          }}
        >
          <ListItemIcon>
            <ExitToAppIcon />
          </ListItemIcon>
          Logout
        </MenuItem>
      </Menu>
    </>
  );
}
