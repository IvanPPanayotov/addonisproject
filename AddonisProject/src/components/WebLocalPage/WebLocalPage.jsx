import uploadWhite from "../../resources/upload-white.png";
import uploadBlack from "../../resources/upload.png";
import puzzlepiece from "../../resources/puzzle-piece.png";
import puzzlePieceWhite from "../../resources/puzzle-piece-white.png";
import useNavigation from "../../hooks/Navigate";
import "./WebLocalPage.css";
import { Typography, Box } from "@mui/material";
import { useTheme } from "../../context/ThemeContext";

const WebLocalPage = () => {
  const { handleNavigation } = useNavigation();
  const { mode } = useTheme();
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        minHeight: "100vh",
      }}
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          gap: "180px",
          p: "20px",
          mb: 25,
        }}
      >
        <Box color={"white"}>
          <img
            src={mode ? puzzlePieceWhite : puzzlepiece}
            alt="Puzzle-piece"
            onClick={() => handleNavigation("/create-addon")}
          />
          <Typography variant="h6" mt={3} color={`text.primary`}>
            Upload from PC
          </Typography>
        </Box>
        <Box color={"white"}>
          <img
            src={mode ? uploadWhite : uploadBlack}
            alt="Upload-icon"
            onClick={() => handleNavigation("/upload-from-web")}
            style={{ cursor: "pointer" }}
          />
          <Typography variant="h6" mt={3} color={`text.primary`}>
            Upload from WEB
          </Typography>
        </Box>
      </Box>
    </div>
  );
};

export default WebLocalPage;
