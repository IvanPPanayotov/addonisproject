import Paper from "@mui/material/Paper";
import InputBase from "@mui/material/InputBase";
import IconButton from "@mui/material/IconButton";
import SearchIcon from "@mui/icons-material/Search";
import { FormControl, MenuItem, Select } from "@mui/material";
import { useState } from "react";
import useNavigation from "../../hooks/Navigate";

import { useEffect } from "react";
import { onSearch } from "../../services/search.service";

export default function SearchBar2({ getSearchDetails, userMode, width }) {
  const [searchText, setSearchText] = useState("");
  const [selectedFilter, setSelectedFilter] = useState("Name");
  const { handleNavigation } = useNavigation();
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async (event) => {
    event.preventDefault();
    if (userMode) {
      const foundUser = await onSearch([], searchText, selectedFilter);
      setSearchResults(foundUser);
      getSearchDetails(foundUser);
    } else if (searchText) {
      return handleNavigation(`/search/${searchText}/Name`);
    } else return handleNavigation(`/search/`);
  };

  useEffect(() => {
    if (userMode) {
      setSelectedFilter(`Username`);
    }
  }, []);

  return (
    <Paper
      component="form"
      sx={{
        boxShadow: "0px 0px 10px rgba(105, 255, 253, 0.70)",
        p: "2px 4px",
        display: "flex",
        alignItems: "center",
        height: `80px`,
        border: `1px solid white`,
        width: width || "700px",
        maxWidth: `1200px`,
        borderRadius: 30,
      }}
    >
      {userMode && (
        <FormControl
          sx={{
            m: 1,
            minWidth: 90,
          }}
        >
          <Select
            defaultValue={`Username`}
            sx={{ borderRadius: 3, bgcolor: `primary.main`, color: `black` }}
            onChange={(e) => setSelectedFilter(e.target.value)}
          >
            <MenuItem value={`Username`}>Username</MenuItem>
            <MenuItem value={`Email`}>Email</MenuItem>
          </Select>
        </FormControl>
      )}
      <InputBase
        onKeyDown={(event) => {
          if (event.key === "Enter") {
            handleSearch(event);
          }
        }}
        sx={{ ml: 10, flex: 1 }}
        placeholder="Search ..."
        value={searchText}
        onChange={(e) => setSearchText(e.target.value)}
      />
      <IconButton
        type="button"
        onClick={(event) => {
          handleSearch(event);
        }}
        sx={{
          p: "10px",
          "&:hover": {
            bgcolor: `primary.main`,
          },
        }}
      >
        <SearchIcon />
      </IconButton>
    </Paper>
  );
}
