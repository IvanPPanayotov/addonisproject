import { Paper, Typography, Avatar, Button, Box } from "@mui/material";
import { useAuth } from "../../context/AuthenticationContext";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import {
  updateAddonStatus,
  declineAddonStatus,
  updateDownloadCount,
} from "../../services/addons.services";
import useNavigation from "../../hooks/Navigate";
import DownloadIcon from "@mui/icons-material/Download";
import { downloadAddon } from "../../services/users.services";
import RatingBtn from "../Rating-Btn/RatingBtn";

const AddonItem = ({ addon, addons, handleGetAddons }) => {
  const { currentUserData } = useAuth();
  const { handleNavigation } = useNavigation();

  const checkIfPending = (status) => {
    if (
      currentUserData &&
      currentUserData.role === "admin" &&
      status === "pending"
    ) {
      return true;
    }
    return false;
  };

  const handleApproveAddon = async () => {
    await updateAddonStatus(addon.addonID);
    handleGetAddons();
  };

  const handleDeclineAddon = async () => {
    await declineAddonStatus(addon.addonID);
    handleGetAddons();
  };

  const handleDownloadIcon = async () => {
    await updateDownloadCount(addon.addonID);
    const downloadURL = await downloadAddon(addon);
    const realDownloadLink = addon.downloadLink;
    window.open(downloadURL, "_blank");
  };

  return (
    <Paper elevation={14} sx={{ borderRadius: 16 }}>
      <Paper
        elevation={4}
        sx={{
          borderRadius: 4,
          border: "1px solid rgba(0, 0, 0, 0.1)",
          boxShadow: "0px 0px 2px rgba(105, 255, 253, 0.60)",
          display: "flex",
          alignItems: `center`,
          flexDirection: `column`,
          justifyContent: "center",
        }}
      >
        <Avatar
          onClick={() => {
            handleNavigation(`/addonDetails/${addon.addonID}`, addon);
          }}
          src={addon.addonPicture}
          style={{ width: "70px", height: "70px" }}
        />
        <Box
          sx={{
            display: "flex",
            alignItems: `center`,
            flexDirection: `column`,
            justifyContent: "center",
          }}
        >
          <Typography variant="h6">
            {addon.addonName.length > 16
              ? addon.addonName.slice(0, 16) + "..."
              : addon.addonName}
          </Typography>
          <Typography variant="subtitle2">
            Released: {addon.date.split(` `)[0]}
          </Typography>
          <Box sx={{}}>
            <RatingBtn addonID={addon.addonID} />
          </Box>
          {!checkIfPending(addon.status) ? (
            <Button onClick={handleDownloadIcon}>
              Download
              <DownloadIcon />
            </Button>
          ) : (
            <Box className={`Approve-Decline-Btns`}>
              <Button onClick={handleApproveAddon}>
                Approve:
                <AddIcon />
              </Button>
              <Button onClick={handleDeclineAddon}>
                Decline:
                <RemoveIcon />
              </Button>
            </Box>
          )}
        </Box>
      </Paper>
    </Paper>
  );
};

export default AddonItem;
