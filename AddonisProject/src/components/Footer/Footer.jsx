import { Box } from "@mui/material";

export default function Footer() {
  return (
    <Box
      sx={{
        mt: `auto`,
        width: `100%`,
        maxWidth: `100%`,
        height: 40,
        bgcolor: `primary.main`,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Box>
        <span style={{ color: "#fff" }}>
          © 2023 FeatureForge. All rights reserved.
        </span>
      </Box>
    </Box>
  );
}
