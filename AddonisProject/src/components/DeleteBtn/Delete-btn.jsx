import DeleteIcon from "@mui/icons-material/Delete";
import { deleteFunc } from "../../services/delete.service";

export default function DeleteButton({
  folder,
  id,
  secondaryKey,
  thirdKey,
  handleDelete,
}) {
  return (
    <div>
      <DeleteIcon
        sx={{ cursor: `pointer` }}
        onClick={async () => {
          await deleteFunc(folder, id, secondaryKey, thirdKey);
          handleDelete && handleDelete();
        }}
      />
    </div>
  );
}
