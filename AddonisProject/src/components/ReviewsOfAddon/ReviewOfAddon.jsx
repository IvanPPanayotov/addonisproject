import { get, ref } from "@firebase/database";
import { Avatar, Box, Button, Divider, Paper, TextField } from "@mui/material";
import { useEffect, useState } from "react";
import db from "../../config/fireBase.config";
import { useAuth } from "../../context/AuthenticationContext";
import SingleComment from "../SingleComment/SingleComment";
import { addComment } from "../../services/addComment.service";

export default function ReviewOfAddon({ addon }) {
  const { currentUserData } = useAuth();
  const [textComment, setTextComment] = useState(``);
  const [postComments, setPostComments] = useState([]);
  const [addedComment, setAddedComment] = useState(false);
  const [deleteComment, setDeleteComment] = useState(false);

  const handleCommentText = (e) => {
    setTextComment(e.target.value);
  };

  const handleCommentReRender = () => {
    setAddedComment(!addedComment);
    setTextComment(``);
  };

  const handleDeleteComments = () => {
    setDeleteComment(!deleteComment);
  };

  useEffect(() => {
    get(ref(db, `addons/${addon.addonID}/comments`)).then((snapshot) => {
      if (!snapshot.exists()) {
        return setPostComments([]);
      }

      const commentsArray = [];

      snapshot.forEach((childSnapshot) => {
        const commentData = childSnapshot.val();
        commentsArray.push(commentData);
      });
      setPostComments(commentsArray);
    });
  }, [addedComment, deleteComment]);

  return (
    <Box
      className={`review`}
      sx={{
        pt: 3,
        display: "flex",
        alignItems: "flex-start",
        width: `90%`,
      }}
    >
      <>
        <Paper
          elevation={4}
          sx={{
            borderRadius: "16px",
            bgcolor: `background.default`,
            padding: "10px",
            marginTop: "10px",
            ml: 10,
            width: `100%`,
          }}
        >
          {postComments.length > 0 &&
            postComments.map((comment, index) => {
              return (
                <SingleComment
                  key={index}
                  comment={comment}
                  addonID={addon.addonID}
                  handleDeleteComments={handleDeleteComments}
                />
              );
            })}
          {currentUserData && (
            <>
              {" "}
              <br />
              <Box sx={{ display: "flex", alignItems: "center" }}>
                <Avatar
                  sx={{ marginRight: "10px" }}
                  alt="User Avatar"
                  src={currentUserData.avatarPhoto}
                />
                <TextField
                  id="outlined-multiline-flexible"
                  label="Write a comment..."
                  value={textComment}
                  multiline
                  maxRows={4}
                  sx={{ width: `60%` }}
                  variant="outlined"
                  onChange={handleCommentText}
                />
              </Box>
              <Box sx={{ textAlign: "right", marginTop: "10px" }}>
                <Button
                  variant="contained"
                  onClick={async () => {
                    await addComment(
                      addon.addonID,
                      textComment,
                      currentUserData.email,
                      currentUserData.avatarPhoto
                    );
                    return handleCommentReRender();
                  }}
                >
                  Post
                </Button>
              </Box>
              <Divider sx={{ marginTop: "10px" }} />
            </>
          )}
        </Paper>
      </>
    </Box>
  );
}
