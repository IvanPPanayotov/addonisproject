import { Box, Chip, Paper, Typography } from "@mui/material";
import { useEffect } from "react";
import { useState } from "react";
import { getLastCommit, getPullRequests } from "../../services/users.services";

export default function OverViewAddon({ addon }) {
  const [lastCommit, setLastCommit] = useState(``);
  const [pullRequests, setPullRequests] = useState(0);

  useEffect(() => {
    const getLastCommitDate = async () => {
      const commitDate = await getLastCommit([addon]);
      const pullRequestCount = await getPullRequests([addon]);
      if (pullRequestCount !== 0) {
        setPullRequests(pullRequestCount);
      }
      setLastCommit(commitDate);
    };
    getLastCommitDate();
  }, []);

  return (
    <Paper
      className={`details`}
      sx={{
        display: "flex",
        bgcolor: `background.default`,
        justifyContent: "space-around",
        alignItems: "flex-start",
      }}>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}>
        <Box sx={{ width: "400px" }}>
          <Typography variant="h4">About {addon.addonName}</Typography>
          <Typography className={`About-text`} sx={{ width: "100%" }}>
            {addon.description}
          </Typography>
        </Box>
        <Box sx={{ mt: 5 }}>
          <Typography variant="h6">Published By</Typography>
          <Typography className={`About-text`} sx={{ width: "100%" }}>
            {addon.author}
          </Typography>
        </Box>
      </Box>

      <Box
        className={`tags`}
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          borderLeft: "2px solid #E9E9E9",
          paddingLeft: 20,
        }}>
        <Box sx={{ maxWidth: `70%` }}>
          <Typography>Tags</Typography>
          {addon.tags &&
            addon.tags.map((tag, index) => {
              return <Chip key={index++} label={`#${tag}`} size="small" sx={{ ml: 0 }} />;
            })}
        </Box>

        <Box className={`ide`} mt={3}>
          <Typography>IDE</Typography>
          <Chip label={addon.IDEName} size="small" />
        </Box>

        <Box className={`moreInfo`} sx={{ mt: 3, display: `flex`, flexDirection: "column" }}>
          <Typography variant="subtitle1">More Information</Typography>
          <Typography variant="caption">Release Date: {addon.date.split(` `)[0]}</Typography>
          <Typography variant="caption">Last commit: {lastCommit}</Typography>
          <Typography variant="caption">Pull requests: {pullRequests}</Typography>
        </Box>
      </Box>
    </Paper>
  );
}
