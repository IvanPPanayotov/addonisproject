import { Box, Rating, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { averageRating, rateAddon } from "../../services/rateAddon.service";
import { useAuth } from "../../context/AuthenticationContext";

export default function RatingBtn({ addonID }) {
  const [value, setValue] = useState(0);
  const [listener, setListener] = useState(false);
  const { currentUserData } = useAuth();

  useEffect(() => {
    const rating = async () => {
      const rate = await averageRating(addonID);
      setValue(rate);
    };
    rating();
  }, [listener, addonID]);

  const handlesRating = async (newValue) => {
    await rateAddon(currentUserData.userUID, addonID, newValue);
    setListener(!listener);
  };

  return (
    <Box sx={{ display: `flex` }}>
      <Rating
        value={value}
        sx={{ width: `130px` }}
        onChange={(event, newValue) => {
          handlesRating(newValue);
        }}
      />
      <Typography variant="subtitle2"> {value.toFixed(0)}/5 Average</Typography>
    </Box>
  );
}
