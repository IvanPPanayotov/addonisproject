import { Button, Box, Typography } from "@mui/material";
import Toolbar from "@mui/material/Toolbar";
import Tooltip from "@mui/material/Tooltip";
import { useState } from "react";
import Login from "../../view/Login/Login";
import { useModal } from "../../context/Open&CloseModals";
import UserProfileCard from "../UserProfileCard/UserProfileCard";
import { useAuth } from "../../context/AuthenticationContext";
import useNavigation from "../../hooks/Navigate";
import logo from "../../resources/Light-Theme-logo-no-background.png";
import logodark from "../../resources/logo-for-dark-theme.png";
import LightModeIcon from "@mui/icons-material/LightMode";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import { useTheme } from "../../context/ThemeContext";

const Header = () => {
  const [isHovered, setIsHovered] = useState(false);

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const { openLogInModal } = useModal();
  const { currentUserData } = useAuth();
  const { mode, toggleMode } = useTheme();
  const { handleNavigation } = useNavigation();

  return (
    <>
      <div className="header" style={{ height: "90px", bgcolor: `red` }}>
        <Box
          sx={{
            bgcolor: `primary.main`,
            top: 0,
            left: 0,
            position: `absolute`,
            width: "100%",
          }}
        >
          <Toolbar>
            <img
              alt="Logo"
              style={{ marginLeft: "3rem", height: "60px", cursor: `pointer` }}
              onClick={() => handleNavigation("/")}
              src={logo}
            />
            <Box
              sx={{
                position: "absolute",
                display: `flex`,
                width: `auto`,
                right: "20px",
                height: `100%`,
              }}
            >
              <Button
                sx={{
                  height: `100%`,
                  borderRadius: 0,
                  boxShadow: "none",
                  borderLeft: "1px solid black",

                  "&:focus": {
                    backgroundColor: "accent.main",
                    outline: "none",
                  },
                }}
                variant="contained"
                onClick={() => toggleMode(!mode)}
              >
                {mode ? <LightModeIcon /> : <DarkModeIcon />}
              </Button>
              <Button
                sx={{
                  height: `100%`,
                  borderRadius: 0,
                  boxShadow: "none",
                  borderLeft: "1px solid black",
                  borderRight: "1px solid black",
                  "&:focus": {
                    backgroundColor: "accent.main",
                    outline: "none",
                  },
                }}
                variant="contained"
                onClick={() => handleNavigation("/about-us")}
              >
                Contacts
              </Button>
              <Button
                sx={{
                  height: `100%`,
                  borderRadius: 0,
                  boxShadow: "none",
                  borderRight: "1px solid black",
                  "&:focus": {
                    backgroundColor: "accent.main",
                    outline: "none",
                    boxShadow: "none",
                  },
                }}
                variant="contained"
                onClick={() => handleNavigation("/search")}
              >
                Explore Addons
              </Button>
              {!currentUserData ? (
                <Button
                  sx={{
                    boxShadow: "none",
                    borderRadius: 0,
                    height: `100%`,
                    borderRight: "1px solid black",
                    "&:focus": {
                      backgroundColor: "accent.main",
                      outline: "none",
                      boxShadow: "none",
                    },
                  }}
                  variant="contained"
                  onClick={() => {
                    openLogInModal();
                  }}
                >
                  Sign In
                </Button>
              ) : (
                <>
                  {currentUserData.userVerified &&
                  !currentUserData.isBlocked ? (
                    <Button
                      sx={{
                        boxShadow: "none",
                        borderRadius: 0,
                        height: `100%`,
                        borderRight: "1px solid black",
                        "&:focus": {
                          backgroundColor: "accent.main",
                          outline: "none",
                          boxShadow: "none",
                        },
                      }}
                      variant="contained"
                      onClick={() => handleNavigation("/create-or-upload")}
                    >
                      Upload Addon
                    </Button>
                  ) : (
                    <Tooltip
                      title={
                        <span style={{ fontSize: "14px" }}>
                          {currentUserData.isBlocked ? (
                            <span>You are blocked</span>
                          ) : (
                            <>
                              You are not Identified.
                              <Button
                                onClick={() =>
                                  handleNavigation("/settings", true)
                                }
                              >
                                {" "}
                                go
                              </Button>
                            </>
                          )}
                        </span>
                      }
                      placement="bottom-end"
                      arrow
                      open={isHovered}
                      onClose={() => setIsHovered(false)}
                      onOpen={() => setIsHovered(true)}
                    >
                      <Box
                        onMouseEnter={handleMouseEnter}
                        sx={{
                          display: "flex",
                          boxShadow: "none",
                          cursor: "pointer",
                          borderRadius: 0,
                          alignItems: "center",
                          height: `100%`,
                          borderRight: "1px solid black",
                          "&:hover": {
                            background: "none",
                            color: "#E8E8E8",
                            outline: "none",
                            boxShadow: "none",
                          },
                        }}
                        variant="contained"
                      >
                        <Typography p={1.5}> Upload Addon</Typography>
                      </Box>
                    </Tooltip>
                  )}

                  <UserProfileCard />
                </>
              )}
            </Box>
          </Toolbar>
        </Box>
        <Login />
      </div>
    </>
  );
};

export default Header;
