import { Button, Box, Typography } from "@mui/material";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import { ref } from "firebase/storage";
import { storage } from "../../../config/fireBase.config";
import { uploadBytes } from "firebase/storage";
import { getDownloadURL } from "firebase/storage";
import { useEffect, useState } from "react";
import {
  getUserDocument,
  uploadProfilePictureData,
  uploadDocumentPictureData,
} from "../../../services/users.services";
import { useAuth } from "../../../context/AuthenticationContext";
import AlertInfoSnackbar from "../../Alert/AlertInfoSnackbar/AlertInfoSnackbar";

export default function UserVerified() {
  const [isProfilePictureUpdated, setIsProfilePictureUpdated] = useState(false);
  const [isDocumentPictureUpdated, setIsDocumentPictureUpdated] =
    useState(false);

  useEffect(() => {
    if (
      currentUserData &&
      currentUserData.verifiedDocuments &&
      currentUserData.verifiedDocuments.profilePicture
    ) {
      setIsProfilePictureUpdated(
        currentUserData.verifiedDocuments.profilePicture
      );
    }
    if (
      currentUserData &&
      currentUserData.verifiedDocuments &&
      currentUserData.verifiedDocuments.documentPicture
    ) {
      setIsDocumentPictureUpdated(
        currentUserData.verifiedDocuments.documentPicture
      );
    }
  }, []);

  const [alertInfo, setAlertInfo] = useState(false);
  const { currentUser, currentUserData, setCurrentUserData } = useAuth();

  const handleProfilePictureChange = (event) => {
    const file = event.target.files[0];
    if (file) {
      if (file.type.startsWith("image/") || file.name.endsWith(".gif")) {
        uploadProfilePicture(file);
        setIsProfilePictureUpdated(true);
      } else {
        setAlertInfo(true);
      }
    }
  };

  const handleDocumentPictureChange = (event) => {
    const file = event.target.files[0];
    if (file) {
      if (file.type.startsWith("image/") || file.name.endsWith(".gif")) {
        uploadDocumentPicture(file);
        setIsDocumentPictureUpdated(true);
      } else {
        setAlertInfo(true);
      }
    }
  };

  const uploadProfilePicture = async (selectedFile) => {
    if (selectedFile) {
      const storageRef = ref(storage, `${selectedFile.name}`);
      await uploadBytes(storageRef, selectedFile);
      const downloadURL = await getDownloadURL(storageRef);
      await uploadProfilePictureData(currentUser.uid, downloadURL);
      const updatedUserData = await getUserDocument(currentUser.uid);
      setCurrentUserData(updatedUserData);
    }
  };

  const uploadDocumentPicture = async (selectedFile) => {
    if (selectedFile) {
      const storageRef = ref(storage, `${selectedFile.name}`);
      await uploadBytes(storageRef, selectedFile);
      const downloadURL = await getDownloadURL(storageRef);
      await uploadDocumentPictureData(currentUser.uid, downloadURL);
      const updatedUserData = await getUserDocument(currentUser.uid);
      setCurrentUserData(updatedUserData);
    }
  };

  return (
    <Box
      className="boxContent"
      sx={{
        justifyContent: "center",
        alignItems: "center",
        alignContent: "center",
        height: "inherit",
      }}
    >
      {alertInfo && (
        <AlertInfoSnackbar message={"Please upload a valid image file"} />
      )}
      <Typography
        sx={{
          padding: "100px 100px",
          width: "75%",
          lineHeight: "3",
          fontSize: "1.1rem",
        }}
      >
        To uphold security standards and ensure the authenticity of our
        platform, we kindly request your cooperation in providing a profile
        picture and an image of an official identification document. These
        measures are crucial in verifying user identities, creating a safe space
        for our community. Your profile picture helps users recognize and
        connect with you, while the identification document prevents fraudulent
        activities. Rest assured, all information provided will be handled
        confidentially and solely used for platform identification. We
        appreciate your cooperation in maintaining a secure environment for all.
        If you have any concerns, please contact our support team.
      </Typography>

      <label htmlFor="profile-picture-input" style={{ cursor: "pointer" }}>
        {!isProfilePictureUpdated ? (
          <input
            id="profile-picture-input"
            type="file"
            onChange={handleProfilePictureChange}
            accept=".jpg, .jpeg, .png, .pdf"
            style={{ display: "none" }}
          />
        ) : (
          <input
            id="profile-picture-input"
            disabled
            accept=".jpg, .jpeg, .png, .pdf"
            style={{ display: "none" }}
          />
        )}

        <Button
          onClick={() => setAlertInfo(false)}
          variant="contained"
          color={!isProfilePictureUpdated ? "primary" : "secondary"}
          component="span"
          sx={{ width: "15vw" }}
        >
          Upload Profile Picture
        </Button>

        {isProfilePictureUpdated && (
          <CheckCircleIcon
            sx={{
              position: "relative",
              left: "-50%",
              top: "50px",
              color: "primary.main",
            }}
          />
        )}
      </label>

      <label htmlFor="document-picture-input" style={{ cursor: "pointer" }}>
        {!isDocumentPictureUpdated ? (
          <input
            id="document-picture-input"
            type="file"
            onChange={handleDocumentPictureChange}
            accept=".jpg, .jpeg, .png, .pdf"
            style={{ display: "none" }}
          />
        ) : (
          <input
            id="document-picture-input"
            disabled
            accept=".jpg, .jpeg, .png, .pdf"
            style={{ display: "none" }}
          />
        )}

        <Button
          onClick={() => setAlertInfo(false)}
          variant="contained"
          color={!isDocumentPictureUpdated ? "primary" : "secondary"}
          component="span"
          sx={{ width: "15vw" }}
        >
          Upload ID picture
        </Button>
        {isDocumentPictureUpdated && (
          <CheckCircleIcon
            sx={{
              position: "relative",
              left: "-50%",
              top: "50px",
              color: "primary.main",
            }}
          />
        )}
      </label>
    </Box>
  );
}
