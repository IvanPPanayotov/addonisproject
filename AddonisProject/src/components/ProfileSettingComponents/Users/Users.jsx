import {
  Avatar,
  ListItemText,
  Typography,
  List,
  Fab,
  ListItem,
  Paper,
} from "@mui/material";
import Tooltip from "@mui/material/Tooltip";
import BlockIcon from "@mui/icons-material/Block";
import SupervisorAccountIcon from "@mui/icons-material/SupervisorAccount";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import PropTypes from "prop-types";
import { blockUser, makeAdmin } from "../../../services/users.services";
import { useState } from "react";
import "./Users.css";

const Users = ({ user, handleGetAllUsers }) => {
  const { avatarPhoto, role, userName, email, isBlocked } = user;

  const [isHovered, setIsHovered] = useState(false);

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  const handleBlockUser = async (user) => {
    const currentStatus = isBlocked === true ? false : true;
    await blockUser(user, currentStatus);
    handleGetAllUsers();
  };

  const handleMakeAdmin = async (user) => {
    const currentRole = role === "admin" ? "member" : "admin";
    await makeAdmin(user, currentRole);
    handleGetAllUsers();
  };

  return (
    <Paper>
      <List
        elevation={2}
        sx={{
          bgcolor: "secondary.main",
          borderRadius: 4,
          boxShadow: "0px 0px 13px rgba(105, 255, 253, 0.70)",
          display: "flex",
        }}
      >
        <ListItem
          alignItems="flex-start"
          sx={{
            width: "190px",
            height: "200px",
            borderRadius: "12px",
            overflow: "hidden",
            justifyContent: "center",
            flexDirection: "column",
            alignItems: "center",
            textAlign: "center",
          }}
        >
          <Avatar
            alt="Profile picture"
            style={{ width: "90px", height: "90px" }}
            src={avatarPhoto}
          />
          <ListItemText
            primary={
              <div
                style={{
                  display: "grid",
                  gridTemplateRows: "auto 1fr auto auto",
                  gap: "4px",
                }}
              >
                <Typography className="customFontSize">{role}</Typography>
                <Typography variant="h6">{userName}</Typography>
                <Tooltip
                  title={<span style={{ fontSize: "14px" }}>{email}</span>}
                  placement="bottom-end"
                  arrow
                  open={isHovered}
                  onClose={() => setIsHovered(false)}
                  onOpen={() => setIsHovered(true)}
                >
                  <Typography
                    onMouseEnter={handleMouseEnter}
                    onMouseLeave={handleMouseLeave}
                    style={{
                      overflow: "hidden",
                      whiteSpace: "nowrap",
                      textOverflow: "ellipsis",
                      transition: "white-space 0.3s ease-in-out",
                    }}
                    variant="body3"
                  >
                    {email}
                  </Typography>
                </Tooltip>

                <Tooltip
                  title={
                    <span
                      style={{
                        fontSize: "15px",
                      }}
                    >
                      {isBlocked ? "Unblock User" : "Block user"}
                    </span>
                  }
                  placement="right"
                  arrow
                >
                  <Fab
                    size="small"
                    color="primary"
                    sx={{
                      position: "absolute",
                      right: "5px",
                      top: "1px",
                    }}
                    onClick={() => handleBlockUser(user)}
                  >
                    {isBlocked ? (
                      <BlockIcon
                        sx={{ fontSize: "30px", color: "red" }}
                      ></BlockIcon>
                    ) : (
                      <BlockIcon sx={{ fontSize: "30px" }}></BlockIcon>
                    )}
                  </Fab>
                </Tooltip>

                <Tooltip
                  title={
                    <span
                      style={{
                        fontSize: "15px",
                      }}
                    >
                      {role === "admin" ? "Make member" : "Make admin"}
                    </span>
                  }
                  placement="right"
                  arrow
                >
                  <Fab
                    color="accent.main"
                    size="small"
                    sx={{
                      position: "absolute",
                      right: "5px",
                      top: "50px",
                    }}
                    onClick={() => handleMakeAdmin(user)}
                  >
                    {role === "admin" ? (
                      <>
                        <SupervisorAccountIcon
                          sx={{ fontSize: "25px", color: "black" }}
                        />
                        <CheckCircleIcon
                          sx={{
                            position: "absolute",
                            right: "-6px",
                            bottom: "-11px",
                            color: "primary.main",
                          }}
                        />
                      </>
                    ) : (
                      <SupervisorAccountIcon
                        sx={{ fontSize: "30px", color: "black" }}
                      />
                    )}
                  </Fab>
                </Tooltip>
              </div>
            }
          />
        </ListItem>
      </List>
    </Paper>
  );
};

Users.propTypes = {
  user: PropTypes.shape({
    avatarPhoto: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    userName: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    isBlocked: PropTypes.bool.isRequired,
  }).isRequired,
};

export default Users;
