import { useEffect } from "react";
import { Typography, TextField, FormControl, Box, Button } from "@mui/material";
import InputAdornment from "@mui/material/InputAdornment";
import PhoneIcon from "@mui/icons-material/Phone";
import { MuiTelInput } from "mui-tel-input";
import { useState } from "react";
import AlertSuccess from "../../Alert/AlertSuccess/AlertSuccess";
import Loading from "../../Loading/Loading";

import {
  validatePhone,
  validateEmail,
  validatePassword,
} from "../../../services/validations";
import { useAuth } from "../../../context/AuthenticationContext";
import {
  getUserDocument,
  updatePhoneData,
} from "../../../services/users.services";
import { useUniqueValues } from "../../../context/UniqueValuesContext";

export default function ProfileUpdate() {
  const [email, setEmail] = useState("");
  const [emailValid, setEmailValid] = useState(true);

  const [phone, setPhone] = useState("");
  const [phoneValid, setPhoneValid] = useState(true);

  const [password, setPassword] = useState("");
  const [isPasswordValid, setIsPasswordValid] = useState(true);

  const [success, setSuccess] = useState(false);
  const [loading, setLoading] = useState(false);

  const { uniqueEmails, uniquePhones } = useUniqueValues();

  const { currentUser, setCurrentUserData, currentUserData, changeEmail } =
    useAuth();

  useEffect(() => {
    setEmail(currentUserData.email);
    setPhone(currentUserData.phoneNumber);
  }, []);

  const handleUpdateEmail = async (event) => {
    event.preventDefault();
    setLoading(true);
    setSuccess(false);

    const isAllValid =
      !uniqueEmails.has(email) &&
      validateEmail(email) &&
      validatePassword(password);

    if (!isAllValid) {
      setEmailValid(validateEmail(email) && !uniqueEmails.has(email));
      setIsPasswordValid(validatePassword(password));
      setLoading(false);
    } else {
      try {
        await changeEmail(email, password);
        const updatedUserData = await getUserDocument(currentUser.uid);
        setLoading(false);
        setCurrentUserData(updatedUserData);
        setSuccess(true);
      } catch (error) {
        setLoading(false);
      }
    }
  };

  const handleUpdatePhone = (event) => {
    event.preventDefault();
    setSuccess(false);

    if (!validatePhone(phone) || uniquePhones.has(phone)) {
      setPhoneValid(validatePhone(phone) && !uniquePhones.has(phone));
    } else {
      setSuccess(true);
      uniquePhones.delete(currentUserData.phoneNumber);
      updatePhoneData(currentUser, phone);
    }
  };

  return (
    <Box
      className="boxContent"
      sx={{ justifyContent: "center", alignItems: "center" }}
    >
      {loading && <Loading />}
      {success && <AlertSuccess message="Update" />}
      <FormControl variant="standard" sx={{ mt: 18, ml: 15, mr: 15 }}>
        <Typography>Phone number</Typography>
        {phoneValid ? (
          <MuiTelInput
            required
            margin="normal"
            sx={{ width: "600px", marginBottom: "1rem" }}
            value={phone}
            label="Phone"
            defaultCountry="BG"
            variant="outlined"
            onChange={(val) => {
              setPhone(val.replace(/ /g, ""));
              setPhoneValid(true);
              setSuccess(false);
            }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <PhoneIcon />
                </InputAdornment>
              ),
            }}
          />
        ) : (
          <MuiTelInput
            sx={{ width: "600px", marginBottom: "1rem" }}
            margin="normal"
            value={""}
            error
            defaultCountry="BG"
            label={uniquePhones.has(phone) ? "Phone exist" : "Invalid phone"}
            variant="outlined"
            onFocus={() => {
              setPhoneValid(true);
              setSuccess(false);
            }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <PhoneIcon />
                </InputAdornment>
              ),
            }}
          />
        )}
        <Button
          variant="contained"
          color="primary"
          type="submit"
          sx={{ mt: 1, mb: 2 }}
          onClick={handleUpdatePhone}
        >
          Update phone
        </Button>
      </FormControl>

      <FormControl variant="standard" sx={{ mt: 4 }}>
        <Typography>Email</Typography>

        {emailValid ? (
          <TextField
            sx={{ width: "600px" }}
            margin="normal"
            required
            id="email"
            label="Email "
            value={email}
            onChange={(event) => {
              setEmail(event.target.value);
              setEmailValid(true);
              setSuccess(false);
            }}
          />
        ) : (
          <TextField
            sx={{ width: "600px" }}
            error
            fullWidth
            margin="normal"
            id="email"
            label={uniqueEmails.has(email) ? "Email exist" : "Invalid email"}
            value={""}
            onFocus={() => {
              setEmailValid(true);
            }}
          />
        )}

        {isPasswordValid ? (
          <TextField
            margin="normal"
            required
            id="password"
            label="Password"
            type="password"
            autoComplete="off"
            fullWidth
            value={password}
            onChange={(event) => {
              setPassword(event.target.value);
              setIsPasswordValid(true);
            }}
          />
        ) : (
          <TextField
            error
            fullWidth
            margin="normal"
            id="password"
            type="password"
            label="Invalid password"
            autoComplete="off"
            value={""}
            onFocus={() => {
              setIsPasswordValid(true);
            }}
          />
        )}

        <Button
          color="primary"
          type="submit"
          variant="contained"
          sx={{ mt: 1, mb: 2 }}
          onClick={handleUpdateEmail}
        >
          Update Email
        </Button>
      </FormControl>
    </Box>
  );
}
