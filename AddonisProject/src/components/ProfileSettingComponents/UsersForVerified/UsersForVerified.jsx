import { useState } from "react";
import {
  Button,
  Dialog,
  List,
  DialogContent,
  Paper,
  DialogTitle,
  Typography,
} from "@mui/material";
import Tooltip from "@mui/material/Tooltip";

import {
  makeIdentifyUser,
  declineUserDocuments,
} from "../../../services/users.services";
import VpnKeyIcon from "@mui/icons-material/VpnKey";
import VpnKeyOffIcon from "@mui/icons-material/VpnKeyOff";

const UsersForVerified = ({ user, handleGetAllUsers }) => {
  const [open, setOpen] = useState(false);
  const [text, setText] = useState("");
  const [selectedImage, setSelectedImage] = useState(null);
  const [alertInfo, setAlertInfo] = useState(false);

  const handleIdentifyUser = () => {
    if (
      user.verifiedDocuments &&
      user.verifiedDocuments.documentPicture &&
      user.verifiedDocuments.profilePicture
    ) {
      makeIdentifyUser(user.userUID);
      handleGetAllUsers();
    } else {
      setAlertInfo(true);
    }
  };

  const handleDeclineUserDocuments = () => {
    declineUserDocuments(user.userUID);
    handleGetAllUsers();
  };

  const handleImageClick = (image, text) => {
    setText(text);
    setSelectedImage(image);
    setOpen(true);
  };

  const handleClose = () => {
    setText("");
    setOpen(false);
    setSelectedImage(null);
  };

  return (
    <>
      <Paper>
        <List
          elevation={2}
          sx={{
            bgcolor: "secondary.main",
            borderRadius: 6,
            boxShadow: "0px 0px 13px rgba(105, 255, 253, 0.70)",
            display: "block",
            flexDirection: "column",
          }}
        >
          <Tooltip
            placement="top"
            arrow
            title={
              alertInfo ? (
                <span
                  style={{
                    fontSize: "18px",
                  }}
                >
                  {"No provided documents"}
                </span>
              ) : (
                <span
                  style={{
                    fontSize: "18px",
                  }}
                >
                  {"Accept identity"}
                </span>
              )
            }
          >
            <span>
              <Button
                onClick={handleIdentifyUser}
                onMouseEnter={() => setAlertInfo(false)}
              >
                <VpnKeyIcon sx={{ cursor: "pointer" }} />
              </Button>
            </span>
          </Tooltip>

          <>
            <Tooltip
              title={
                <span
                  style={{
                    fontSize: "18px",
                  }}
                >
                  {user.verifiedDocuments ? "Decline documents" : null}
                </span>
              }
              placement="top"
              arrow
            >
              <span>
                <Button
                  onClick={handleDeclineUserDocuments}
                  disabled={!user.verifiedDocuments}
                >
                  {user.verifiedDocuments ? (
                    <VpnKeyOffIcon
                      sx={{ cursor: "pointer", color: "#f23427" }}
                    />
                  ) : (
                    <VpnKeyOffIcon sx={{ cursor: "pointer", color: "grey" }} />
                  )}
                </Button>
              </span>
            </Tooltip>
          </>

          <Typography variant="h6">{user.email}</Typography>
          <>
            <Button
              onClick={() =>
                handleImageClick(
                  user?.verifiedDocuments?.profilePicture,
                  user?.verifiedDocuments?.profilePicture
                    ? "View Profile Picture"
                    : "Not Uploaded"
                )
              }
            >
              View Profile Picture
            </Button>

            <Button
              onClick={() =>
                handleImageClick(
                  user?.verifiedDocuments?.documentPicture,
                  user?.verifiedDocuments?.documentPicture
                    ? "View Document Picture"
                    : "Not Uploaded"
                )
              }
            >
              View Document Picture
            </Button>
          </>
          <Dialog open={open} onClose={handleClose}>
            <DialogTitle sx={{ textAlign: "center" }}>{text}</DialogTitle>
            <DialogContent sx={{ textAlign: "center" }}>
              {selectedImage && (
                <img
                  src={selectedImage}
                  alt="Selected"
                  style={{ maxWidth: "100%", maxHeight: "100%" }}
                />
              )}
            </DialogContent>
          </Dialog>
        </List>
      </Paper>
    </>
  );
};

export default UsersForVerified;
