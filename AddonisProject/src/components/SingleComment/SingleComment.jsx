import { useEffect, useState } from "react";
import { Avatar, Box, Paper, Typography } from "@mui/material";
import { useAuth } from "../../context/AuthenticationContext";
import DeleteButton from "../DeleteBtn/Delete-btn";

const SingleComment = ({ comment, addonID, handleDeleteComments }) => {
  const { currentUserData } = useAuth();
  const [deleteComments, setDeleteComments] = useState(false);

  useEffect(() => {
    if (currentUserData) {
      if (
        currentUserData.email === comment.author ||
        currentUserData.role === `admin`
      ) {
        setDeleteComments(true);
      }
    }
  }, []);

  return (
    <Paper
      sx={{
        borderRadius: "16px",
        bgcolor: `background.default`,
        padding: "10px",
        marginTop: "10px",
        position: "relative", // To position the delete button
      }}
    >
      <Box sx={{ display: "flex", alignItems: "center" }}>
        <Avatar
          sx={{ marginRight: "10px" }}
          alt="User Avatar"
          src={comment.authorAvatar}
        />
        <Box sx={{ flexGrow: 1 }}>
          <Typography variant="body2" color="text.primary">
            <strong>{comment.author}</strong> - {comment.createdOn}
          </Typography>
          <Typography variant="body2" color="text.primary">
            {comment.comment}
          </Typography>
        </Box>
        {deleteComments && (
          <DeleteButton
            sx={{ position: "absolute", top: 0, right: 0 }}
            folder={`addons`}
            id={addonID}
            secondaryKey={`comments`}
            thirdKey={comment.commentID}
            handleDelete={handleDeleteComments}
          ></DeleteButton>
        )}
      </Box>
    </Paper>
  );
};

export default SingleComment;
