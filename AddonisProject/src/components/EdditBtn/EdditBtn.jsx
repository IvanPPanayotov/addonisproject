import { useState } from "react";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Fade from "@mui/material/Fade";
import EditIcon from "@mui/icons-material/Edit";
import Tooltip from "@mui/material/Tooltip";
import { useAuth } from "../../context/AuthenticationContext";
import { deleteFromGithub, selectAddon } from "../../services/addons.services";
import useNavigation from "../../hooks/Navigate";
import { deleteFromUser, deleteFunc } from "../../services/delete.service";

const EditPost = ({ addon }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const { handleNavigation } = useNavigation();

  const { currentUserData } = useAuth();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleUpdateBtn = async () => {
    handleNavigation("/update-addon/", { addon: addon });
  };

  const handleFeature = async () => {
    selectAddon(addon.addonID);
  };

  const handleDeleteButton = async () => {
    await deleteFromGithub(addon.GitHubName, addon.SHA, currentUserData.email);
    await deleteFunc(`addons`, addon.addonID);
    handleNavigation(`/home`);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleCheckIfAdminOrAuthor = () => {
    if (currentUserData.role === `admin` || currentUserData.email === addon.author) {
      return true;
    }
    return false;
  };

  return (
    <div>
      {currentUserData && handleCheckIfAdminOrAuthor() && (
        <>
          <Tooltip title={currentUserData.isBlocked ? "You are blocked" : ""}>
            <span>
              <Button
                id="fade-button"
                aria-controls={open ? "fade-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={open ? "true" : undefined}
                sx={{
                  bgcolor: "rgba(128, 128, 128, 0.5)",
                  pointerEvents: currentUserData.isBlocked ? "none" : "auto",
                }}
                onClick={handleClick}
                disabled={currentUserData.isBlocked}>
                <EditIcon />
              </Button>
            </span>
          </Tooltip>
          <Menu
            id="fade-menu"
            MenuListProps={{
              "aria-labelledby": "fade-button",
            }}
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            TransitionComponent={Fade}>
            <MenuItem
              onClick={() => {
                handleUpdateBtn();
              }}>
              Edit
            </MenuItem>
            <MenuItem
              onClick={() => {
                handleDeleteButton();
              }}>
              Delete
            </MenuItem>
            {currentUserData.role === `admin` && (
              <MenuItem
                onClick={() => {
                  handleFeature();
                }}>
                Feature
              </MenuItem>
            )}
          </Menu>
        </>
      )}
    </div>
  );
};

export default EditPost;
