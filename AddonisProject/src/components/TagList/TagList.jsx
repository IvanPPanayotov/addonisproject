import { useEffect, useState } from "react";
import { Autocomplete, TextField } from "@mui/material";
import { getAllTags } from "../../services/tags.services";

export default function TagList({ changeTag }) {
  const [tags, setTags] = useState([]);
  const [selectedTag, setSelectedTag] = useState("");

  const handleTagChange = (event, newValue) => {
    setSelectedTag(newValue);
    changeTag(newValue);
  };

  useEffect(() => {
    const getAllKeys = async () => {
      const keys = await getAllTags();
      setTags(keys);
    };
    getAllKeys();
  }, []);

  return (
    <Autocomplete
      value={selectedTag}
      onChange={handleTagChange}
      options={tags}
      freeSolo
      renderInput={(params) => (
        <TextField
          {...params}
          label="Select or add a tag"
          variant="outlined"
          InputProps={{
            ...params.InputProps,
            startAdornment: (
              <>
                <span
                  style={{
                    marginLeft: "10px",
                    marginRight: "5px",
                    color: "#aaa",
                  }}>
                  #
                </span>
              </>
            ),
          }}
        />
      )}
      renderOption={(props, option) => (
        <li {...props} style={{ listStyle: "none", padding: "8px" }}>
          {option}
        </li>
      )}
      isOptionEqualToValue={(option, value) => option.toLowerCase() === value.toLowerCase()}
      getOptionLabel={(option) => option}
      style={{ width: "100%" }}
    />
  );
}
