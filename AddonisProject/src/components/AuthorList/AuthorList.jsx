import { useEffect, useState } from "react";
import { Autocomplete, TextField } from "@mui/material";
import {
  getAddonsFromUserName,
  getAllAuthors,
} from "../../services/users.services";

export default function AuthorList({ changeAuthor }) {
  const [authors, setAuthors] = useState([]);
  const [selectedAuthor, setSelectedAuthor] = useState("");

  const handleTagChange = async (event, newValue) => {
    setSelectedAuthor(newValue);
    if (!newValue) {
      changeAuthor(null);
      return;
    }
    const userAddons = await getAddonsFromUserName(newValue);
    changeAuthor(userAddons);
  };

  useEffect(() => {
    const getAllKeys = async () => {
      const keys = await getAllAuthors();
      setAuthors(keys);
    };
    getAllKeys();
  }, []);

  return (
    <Autocomplete
      value={selectedAuthor}
      onChange={handleTagChange}
      options={authors}
      freeSolo
      renderInput={(params) => (
        <TextField
          {...params}
          label="Author"
          variant="outlined"
          InputProps={{
            ...params.InputProps,
          }}
        />
      )}
      renderOption={(props, option) => (
        <li {...props} style={{ listStyle: "none", padding: "8px" }}>
          {option}
        </li>
      )}
      isOptionEqualToValue={(option, value) =>
        option.toLowerCase() === value.toLowerCase()
      }
      getOptionLabel={(option) => option}
      style={{ width: "100%" }}
    />
  );
}
