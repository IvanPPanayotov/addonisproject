import { useState } from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import {
  Button,
  Select,
  MenuItem,
  Typography,
  FormControl,
  InputLabel,
  FormHelperText,
  Paper,
} from "@mui/material";

import Loading from "../Loading/Loading";
import AlertInfoSnackbar from "../Alert/AlertInfoSnackbar/AlertInfoSnackbar";
import { uploadAddonService } from "../../services/users.services";
import { useAuth } from "../../context/AuthenticationContext";
import CloudUploadIcon from "@mui/icons-material/CloudUpload";
import useNavigation from "../../hooks/Navigate";
import { ref } from "firebase/storage";
import { storage } from "../../config/fireBase.config";
import { uploadBytes } from "firebase/storage";
import { getDownloadURL } from "firebase/storage";

import {
  validateAddonName,
  validateAddonDescription,
  validateTags,
  isValidURL,
} from "../../services/validations";
import { useUniqueValues } from "../../context/UniqueValuesContext";

const UploadAddonFromWeb = () => {
  const [addonName, setAddonName] = useState("");
  const [addonDescription, setAddonDescription] = useState("");
  const [tags, setTags] = useState([]);
  const [IDEName, setIDEName] = useState("");
  const [originLink, setOriginLink] = useState("");
  const [uploadedPicture, setUploadedPicture] = useState();
  const { currentUserData, currentUser } = useAuth();
  const { handleNavigation } = useNavigation();
  const [addonNameValid, setAddonNameValid] = useState(true);
  const [addonDescriptionValid, setAddonDescriptionValid] = useState(true);
  const [addonTagsValid, setAddonTagsValid] = useState(true);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [alertInfo, setAlertInfo] = useState(false);
  const [originLinkValid, setOriginLinkValid] = useState(true);
  const isIDENameValid = IDEName !== "";

  const { uniqueAddonName } = useUniqueValues();

  const handleUploadButton = () => {
    const isAllValid =
      validateAddonName(addonName) &&
      !uniqueAddonName.has(addonName) &&
      validateAddonDescription(addonDescription) &&
      validateTags(tags) &&
      isValidURL(originLink) &&
      isIDENameValid;

    if (!isAllValid) {
      setAddonNameValid(
        validateAddonName(addonName) && !uniqueAddonName.has(addonName)
      );
      setAddonDescriptionValid(validateAddonDescription(addonDescription));
      setAddonTagsValid(validateTags(tags));
      setOriginLinkValid(isValidURL(originLink));
      setError(true);
    } else {
      handleFileUpload();
    }
  };

  const handleFileUpload = async () => {
    setLoading(true);
    const downloadURL = await storageUpload(uploadedPicture);
    uploadAddonService(
      currentUser.uid,
      currentUserData.avatarPhoto,
      currentUserData.email,
      downloadURL,
      addonName,
      addonDescription,
      originLink,
      IDEName,
      tags,
      originLink,
      null,
      ""
    );
    setLoading(false);
    handleNavigation("/");
  };

  const storageUpload = async (uploadedPicture) => {
    if (uploadedPicture) {
      const storageRef = ref(storage, `${uploadedPicture.name}`);
      await uploadBytes(storageRef, uploadedPicture);
      const downloadURL = await getDownloadURL(storageRef);
      return downloadURL;
    }
    return null;
  };
  const handleTagInput = (event) => {
    const inputTags = event.target.value
      .split("#")
      .map((tag) => tag.trim())
      .filter((tag) => tag !== "");

    setTags(inputTags);
  };

  const handlePictureChange = (e) => {
    const file = e.target.files[0];

    if (file) {
      if (file.type.startsWith("image/") || file.name.endsWith(".gif")) {
        setUploadedPicture(file);
      } else {
        setAlertInfo(true);
      }
    }
  };

  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        minHeight: "100vh",
      }}
    >
      {loading && <Loading />}
      <Paper
        sx={{
          width: "50%",
          borderRadius: "30px",
          p: 3,
          boxShadow: "0px 0px 10px rgba(105, 255, 253, 0.70)",
        }}
      >
        <Typography
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          Create Addon
        </Typography>
        {addonNameValid ? (
          <TextField
            label="Name"
            variant="outlined"
            fullWidth
            sx={{ borderRadius: "30px", marginBottom: "1rem" }}
            onChange={(event) => setAddonName(event.target.value)}
          />
        ) : (
          <TextField
            fullWidth
            error
            margin="normal"
            id="addonName"
            label={
              uniqueAddonName.has(addonName)
                ? "Name exist"
                : "Between 2-20 symbols"
            }
            value={""}
            onFocus={() => {
              setAddonNameValid(true);
              setError(false);
            }}
          />
        )}
        {addonDescriptionValid ? (
          <TextField
            label="Description"
            variant="outlined"
            fullWidth
            sx={{ borderRadius: "30px", marginBottom: "1rem" }}
            onChange={(event) => setAddonDescription(event.target.value)}
          />
        ) : (
          <TextField
            fullWidth
            error
            margin="normal"
            id="addonDescription"
            label={"Between 16-180 symbols"}
            value={""}
            onFocus={() => {
              setAddonDescriptionValid(true);
              setError(false);
            }}
          />
        )}

        <FormControl
          variant="outlined"
          fullWidth
          required
          error={!isIDENameValid}
        >
          <InputLabel>IDE Name</InputLabel>
          <Select
            value={IDEName}
            onChange={(event) => setIDEName(event.target.value)}
            label=""
          >
            <MenuItem value="">
              <em>Select IDE</em>
            </MenuItem>
            <MenuItem value="Visual Studio Code">Visual Studio Code</MenuItem>
            <MenuItem value="WebStorm">WebStorm</MenuItem>
            <MenuItem value="Sublime Text">Sublime Text</MenuItem>
          </Select>
          {!isIDENameValid && (
            <FormHelperText style={{ color: "orange" }}>
              Please select an IDE.
            </FormHelperText>
          )}
        </FormControl>

        {originLinkValid ? (
          <TextField
            label="Origin Link"
            variant="outlined"
            fullWidth
            sx={{ borderRadius: "30px", marginBottom: "1rem" }}
            onChange={(event) => setOriginLink(event.target.value)}
          />
        ) : (
          <TextField
            fullWidth
            error
            margin="normal"
            id="originLink"
            label={"Enter a valid Github repository Link!"}
            value={""}
            onFocus={() => {
              setOriginLinkValid(true);
              setError(false);
            }}
          />
        )}

        {addonTagsValid ? (
          <TextField
            label="Tags"
            variant="outlined"
            fullWidth
            sx={{ borderRadius: "30px", marginBottom: "1rem" }}
            onChange={handleTagInput}
          />
        ) : (
          <TextField
            fullWidth
            error
            margin="normal"
            id="originLink"
            label={"You must add a tag!"}
            value={""}
            onFocus={() => {
              setAddonTagsValid(true);
              setError(false);
            }}
          />
        )}

        {alertInfo && (
          <AlertInfoSnackbar message={"Please upload a valid image file"} />
        )}
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            marginBottom: "1rem",
          }}
        >
          <label htmlFor="picture-input" style={{ cursor: "pointer" }}>
            <input
              id="picture-input"
              type="file"
              onChange={handlePictureChange}
              accept=".jpg, .jpeg, .png, .pdf"
              style={{ display: "none" }}
            />
            <Button
              onClick={() => setAlertInfo(false)}
              variant="contained"
              color="secondary"
              component="span"
              startIcon={<CloudUploadIcon />}
            >
              Upload Picture
            </Button>
          </label>
        </Box>

        <Button variant="contained" onClick={handleUploadButton} fullWidth>
          Upload
        </Button>
      </Paper>
    </Box>
  );
};

export default UploadAddonFromWeb;
