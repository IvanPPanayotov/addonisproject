import { useState } from "react";
import { Autocomplete, TextField } from "@mui/material";

export default function IdeList({ changeIDE }) {
  const [selectedAuthor, setSelectedAuthor] = useState("");

  const handleTagChange = async (event, newValue) => {
    setSelectedAuthor(newValue);
    if (!newValue) {
      changeIDE(null);
      return;
    }
    changeIDE(newValue);
  };

  return (
    <Autocomplete
      value={selectedAuthor}
      onChange={handleTagChange}
      options={[`Sublime Text`, `WebStorm`, `Visual Studio Code`]}
      freeSolo
      renderInput={(params) => (
        <TextField
          {...params}
          label="IDE"
          variant="outlined"
          InputProps={{
            ...params.InputProps,
          }}
        />
      )}
      renderOption={(props, option) => (
        <li {...props} style={{ listStyle: "none", padding: "8px" }}>
          {option}
        </li>
      )}
      isOptionEqualToValue={(option, value) =>
        option.toLowerCase() === value.toLowerCase()
      }
      getOptionLabel={(option) => option}
      style={{ width: "100%" }}
    />
  );
}
