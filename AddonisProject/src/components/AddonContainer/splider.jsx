import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/react-splide/css/sea-green";
import AddonItem from "../AddonItem/AddonItem";
import { Paper, Typography } from "@mui/material";

const Slider = ({ title, addons }) => {
  return (
    <>
      <Typography variant="h5" sx={{ mt: 3, color: "text.primary" }}>
        {title}
      </Typography>
      <Paper
        sx={{
          borderRadius: `30px`,
          m: 3,
          ml: 15,
          mr: 15,
          boxShadow: "0px 0px 10px rgba(105, 255, 253, 0.60)",
        }}
      >
        <Splide
          options={{
            perPage: 5,
            gap: "1rem",
            perMove: 1,
            // autoplay: true,
            pauseOnHover: true,
            resetProgress: false,
            interval: 10000,
          }}
        >
          {addons.map((addon, index) => (
            <SplideSlide key={index}>
              <AddonItem addon={addon} addons={addons} />
            </SplideSlide>
          ))}
        </Splide>
      </Paper>
    </>
  );
};

export default Slider;
