import { createTheme } from "@mui/material/styles";

/**
 * Creates a custom theme using the createTheme function from the @mui/material/styles library.
 * @param {boolean} mode - Determines whether the theme should be light or dark.
 * @returns {object} - A custom theme object based on the provided mode parameter.
 */
const CustomTheme = (mode) => {
  return createTheme({
    palette: {
      mode: mode ? "dark" : "light",
      background: {
        default: mode ? "#131417" : "#FFFFFF",
        paper: mode ? "#121212" : "#f0f1cb",
      },
      primary: {
        main: "#47CF73",
      },
      secondary: {
        main: mode ? "#2C303A" : "#e3e6e6",
      },
      accent: {
        main: mode ? "#E4D944" : "#7d7612",
      },
      text: {
        primary: mode ? "#FFFFFF" : "#000000",
      },
    },
    typography: {
      fontFamily: "Arial, sans-serif",
    },
  });
};

export default CustomTheme;
