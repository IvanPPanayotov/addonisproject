import { Route, Routes } from "react-router-dom";
import ProfileSettingsView from "../view/ProfileSettingsView/ProfileSettingsView";
import Home from "../view/Home/Home";
import CreateAddon from "../components/Create Addon/CreateAddon";
import SingleAddonView from "../view/SingleAddonView/SingleAddonView";
import UpdateAddonView from "../view/UpdateAddonView/UpdateAddonView";
import SearchContentView from "../view/SearchContent/SearchContentView";
import WebLocalPage from "../components/WebLocalPage/WebLocalPage";
import UploadAddonFromWeb from "../components/UploadAddonFromWeb/UploadAddonFromWeb";
import Signup from "../view/SignUp/signUp";
import { useAuth } from "../context/AuthenticationContext";
import AboutUs from "../view/AboutUs/AboutUs";

const Routing = () => {
  const { currentUser } = useAuth();
  const checkIfLoggedIn = () => {
    if (currentUser) {
      return true;
    }
    return false;
  };

  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="*" element={<Home />} />

      {checkIfLoggedIn() && (
        <>
          <Route path="/settings" element={<ProfileSettingsView />} />
          <Route path="/create-addon" element={<CreateAddon />} />
          <Route path="/upload-from-web" element={<UploadAddonFromWeb />} />
          <Route path="/create-or-upload" element={<WebLocalPage />} />
          <Route path="/update-addon" element={<UpdateAddonView />} />
          <Route path="/about-us" element={<AboutUs />} />
        </>
      )}
      {!checkIfLoggedIn() && (
        <>
          <Route path="/register" element={<Signup />} />
        </>
      )}
      <Route path="/addonDetails/:addonID" element={<SingleAddonView />} />
      <Route
        path="/search/:searchContent?/:searchBy?"
        element={<SearchContentView />}
      />
    </Routes>
  );
};

export default Routing;
