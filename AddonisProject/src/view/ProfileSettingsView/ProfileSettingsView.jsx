import {
  Box,
  Paper,
  Divider,
  Avatar,
  Button,
  Typography,
  InputLabel,
  Input,
  Pagination,
} from "@mui/material";
import "./ProfileSettingsView.css";
import AlertInfoSnackbar from "../../components/Alert/AlertInfoSnackbar/AlertInfoSnackbar";
import { useAuth } from "../../context/AuthenticationContext";
import AddonItem from "../../components/AddonItem/AddonItem";
import { getAllAddonsFromDatabase } from "../../services/addons.services";
import { useEffect, useState } from "react";
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";
import ProfileUpdate from "../../components/ProfileSettingComponents/ProfileUpdate/ProfileUpdate";
import Users from "../../components/ProfileSettingComponents/Users/Users";
import {
  changePicture,
  getAllUsers,
  getUserDocument,
} from "../../services/users.services";
import { storage } from "../../config/fireBase.config";
import SearchBar2 from "../../components/SearchBar/SearchBar2";
import InviteFriendComponent from "../InviteFriend/InviteFriend";
import UserVerified from "../../components/ProfileSettingComponents/UserVerified/UserVerified";
import UsersForVerified from "../../components/ProfileSettingComponents/UsersForVerified/UsersForVerified";
import { useLocation } from "react-router-dom";

const ProfileSettingsView = () => {
  const [allUsers, setAllUsers] = useState([]);
  const [usersForIdentify, setUsersForIdentify] = useState([]);
  const [copyUsers, setCopyUsers] = useState([]);
  const [allAddons, setAllAddons] = useState([]);
  const [forApprovalAddons, setForApprovalAddons] = useState([]);
  const [myAddons, setMyAddons] = useState([]);

  const [alertInfo, setAlertInfo] = useState(false);

  const [searchText, setSearchText] = useState("");
  const [searchDetails, setSearchDetails] = useState();

  const [showEditProfileMenu, setShowEditProfileMenu] = useState(true);
  const [showMyAddonsMenu, setShowMyAddonsMenu] = useState(false);
  const [showUsersMenu, setShowUsersMenu] = useState(false);
  const [showUsersForIdentifyMenu, setShowUsersForIdentifyMenu] =
    useState(false);
  const [showApprovalMenu, setShowApprovalMenu] = useState(false);
  const [showVerifiedUserMenu, setShowVerifiedUserMenu] = useState(false);

  const [currentPageUser, setCurrentPageUser] = useState(1);
  const [currentPageAddon, setCurrentPageAddon] = useState(1);
  const [currentPageMyAddons, setCurrentPageMyAddons] = useState(1);
  const [isInviteAFriendVisible, setIsInviteAFriendVisible] = useState(false);

  const postsPerPage = 6;
  const startIndexUsers = (currentPageUser - 1) * postsPerPage;
  const endIndexUsers = Math.min(
    startIndexUsers + postsPerPage,
    copyUsers.length
  );
  const usersToDisplay = copyUsers.slice(startIndexUsers, endIndexUsers);

  const startIndexAddonsForApproval = (currentPageAddon - 1) * postsPerPage;
  const endIndexForApproval = Math.min(
    startIndexAddonsForApproval + postsPerPage,
    forApprovalAddons.length
  );
  const addonsForApprovalToDisplay = forApprovalAddons.slice(
    startIndexAddonsForApproval,
    endIndexForApproval
  );

  const startIndexMyAddons = (currentPageMyAddons - 1) * postsPerPage;
  const endIndexMyAddons = Math.min(
    startIndexMyAddons + postsPerPage,
    myAddons.length
  );

  const myAddonsToDisplay = myAddons.slice(
    startIndexMyAddons,
    endIndexMyAddons
  );

  const location = useLocation();

  const toggleArray = [
    setShowEditProfileMenu,
    setShowMyAddonsMenu,
    setShowUsersMenu,
    setShowApprovalMenu,
    setIsInviteAFriendVisible,
    setShowVerifiedUserMenu,
    setShowUsersForIdentifyMenu,
  ];

  useEffect(() => {
    handleGetAllUsers();
    handleGetAddons();
  }, []);

  useEffect(() => {
    if (location.state) {
      setShowVerifiedUserMenu(true);
      setShowEditProfileMenu(false);
    }
  }, []);

  const { currentUserData, setCurrentUserData, currentUser, isAdmin } =
    useAuth();

  const toggleMode = (goToPage, setCurrentPage) => {
    if (goToPage === true) {
      return;
    }
    toggleArray.map((page) => {
      page((prevPage) => (prevPage !== true ? false : !prevPage));
    });
    setCurrentPage(true);
  };

  const handlePageChangeUser = (event, newPage) => {
    setCurrentPageUser(newPage);
  };

  const handlePageChangeAddon = (event, newPage) => {
    setCurrentPageAddon(newPage);
  };

  const handlePageChangeMyAddon = (event, newPage) => {
    setCurrentPageMyAddons(newPage);
  };

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    if (file) {
      if (file.type.startsWith("image/") || file.name.endsWith(".gif")) {
        handleUpload(file);
      } else {
        setAlertInfo(true);
      }
    }
  };

  const handleUpload = async (selectedFile) => {
    if (selectedFile) {
      const storageRef = ref(storage, `${selectedFile.name}`);
      await uploadBytes(storageRef, selectedFile);
      const downloadURL = await getDownloadURL(storageRef);
      await changePicture(currentUser.uid, downloadURL);
      const updatedUserData = await getUserDocument(currentUser.uid);
      setCurrentUserData(updatedUserData);
    }
  };

  const handleGetAddons = async () => {
    const addons = await getAllAddonsFromDatabase();
    setAllAddons(addons);
    setForApprovalAddons(addons.filter((addon) => addon.status === "pending"));
    setMyAddons(
      addons.filter(
        (addon) =>
          addon.author === currentUserData.email && addon.status === "approved"
      )
    );
  };

  const handleGetAllUsers = async () => {
    const users = await getAllUsers();
    const userData = users.map((user) => user.data);
    setAllUsers(userData);
    setUsersForIdentify(userData.filter((user) => user.userVerified === false));
    setCopyUsers(
      userData.filter((user) => user.email != currentUserData.email)
    );
  };

  const getSearchDetails = (details) => {
    setCopyUsers(details);
    setSearchDetails(details);
    setSearchText("");
    setCurrentPageUser(1);
  };

  return (
    currentUserData && (
      <Paper elevation={0} className="boxContainer" sx={{ bgcolor: `background.default` }}>
        <Box className="boxMenuNav">
          <Avatar
            alt="User Avatar"
            src={currentUserData.avatarPhoto}
            sx={{
              width: "136px",
              height: "136px",
              marginBottom: "16px",
              marginTop: "36px",
            }}
          />
          {alertInfo && (
            <AlertInfoSnackbar message={"Please upload a valid image file"} />
          )}

          <Typography sx={{ marginBottom: "20px" }}>
            <b>Role: {currentUserData.role}</b>
          </Typography>

          <Box className="usernameInfo">
            <Typography>
              <b>{currentUserData.userName}</b>
            </Typography>

            <Button
              sx={{ float: "right", marginRight: "15px" }}
              onClick={() =>
                toggleMode(showEditProfileMenu, setShowEditProfileMenu)
              }
            >
              {" "}
              Edit profile
            </Button>

            {currentUserData.userVerified ? (
              <Button
                sx={{ float: "right", marginRight: "15px" }}
                onClick={() => setAlertInfo(false)}
              >
                <InputLabel style={{ cursor: "pointer", color: "#47CF73" }}>
                  <Input
                    type="file"
                    onChange={handleFileChange}
                    inputProps={{
                      accept: "image/*",
                      style: { display: "none" },
                    }}
                  />
                  Change Avatar
                </InputLabel>
              </Button>
            ) : (
              <Button
                sx={{ float: "right", marginRight: "15px" }}
                onClick={() => setAlertInfo(false)}
              >
                <InputLabel style={{ cursor: "pointer", color: "#47CF73" }}>
                  <Input
                    onClick={() =>
                      toggleMode(showVerifiedUserMenu, setShowVerifiedUserMenu)
                    }
                    inputProps={{
                      accept: "image/*",
                      style: { display: "none" },
                    }}
                  />
                  Identify myself
                </InputLabel>
              </Button>
            )}

            <Typography>
              <b>{currentUserData.email}</b>
            </Typography>
          </Box>

          <Box className="buttonsBox">
            <Button
              sx={{ mt: 1, width: "85%" }}
              variant="contained"
              onClick={() => toggleMode(showMyAddonsMenu, setShowMyAddonsMenu)}
            >
              My addons
            </Button>
            <Button
              sx={{ mt: 1, width: "85%" }}
              variant="contained"
              onClick={() =>
                toggleMode(isInviteAFriendVisible, setIsInviteAFriendVisible)
              }
            >
              Invite a friend
            </Button>

            {isAdmin && (
              <>
                <Button
                  sx={{ mt: 1, width: "85%" }}
                  variant="contained"
                  onClick={() =>
                    toggleMode(
                      showUsersForIdentifyMenu,
                      setShowUsersForIdentifyMenu
                    )
                  }
                >
                  Users for Identify
                </Button>

                <Button
                  sx={{ mt: 1, width: "85%" }}
                  variant="contained"
                  onClick={() =>
                    toggleMode(showApprovalMenu, setShowApprovalMenu)
                  }
                >
                  Addons for approval
                </Button>

                <Button
                  sx={{ mt: 1, width: "85%" }}
                  variant="contained"
                  onClick={() => toggleMode(showUsersMenu, setShowUsersMenu)}
                >
                  Users
                </Button>
              </>
            )}
          </Box>
        </Box>

        <Divider
          className="dividerStyles"
          sx={{ margin: "50px 0" }}
          orientation="vertical"
          flexItem
        />
        {isInviteAFriendVisible && <InviteFriendComponent />}
        {showEditProfileMenu && <ProfileUpdate />}
        {showVerifiedUserMenu && <UserVerified />}

        {showMyAddonsMenu && (
          <Box
            className="boxContent"
            sx={{
              display: `flex`,
              justifyContent: "space-around",
              flexDirection: `column`,
              height: "auto",
            }}
          >
            <Box
              className="boxContent"
              sx={{
                justifyContent: "center",
                alignContent: "center",
                gap: "5vw",
              }}
            >
              {myAddons &&
                myAddonsToDisplay.map((addon, index) => (
                  <Box key={index}>
                    <AddonItem addon={addon} />
                  </Box>
                ))}
            </Box>
            <Pagination
              count={Math.ceil(myAddons.length / postsPerPage)}
              page={currentPageMyAddons}
              onChange={handlePageChangeMyAddon}
              color="primary"
              sx={{ display: "flex", justifyContent: "center" }}
            />
          </Box>
        )}

        {showUsersForIdentifyMenu && (
          <Box className="boxContent" sx={{ justifyContent: "center" }}>
            {usersForIdentify.map((user, index) => (
              <Box key={index} sx={{ mr: 10, mt: 5 }}>
                <UsersForVerified
                  user={user}
                  handleGetAllUsers={handleGetAllUsers}
                />
              </Box>
            ))}
          </Box>
        )}

        {showApprovalMenu && (
          <Box
            className="boxContent"
            sx={{
              display: `flex`,
              justifyContent: "space-around",
              flexDirection: `column`,
              height: "auto",
            }}
          >
            <Box
              className="boxContent"
              sx={{
                justifyContent: "center",
                alignContent: "center",
                gap: "5vw",
              }}
            >
              {addonsForApprovalToDisplay
                .filter((addon) => addon.status !== "approved")
                .map((addon, index) => (
                  <AddonItem
                    key={index}
                    addon={addon}
                    handleGetAddons={handleGetAddons}
                  />
                ))}
            </Box>
            <Pagination
              count={Math.ceil(forApprovalAddons.length / postsPerPage)}
              page={currentPageAddon}
              onChange={handlePageChangeAddon}
              color="primary"
              sx={{ display: "flex", justifyContent: "center" }}
            />
          </Box>
        )}

        {showUsersMenu && (
          <>
            <Box
              className="boxContent"
              sx={{
                display: `flex`,
                justifyContent: "space-around",
                flexDirection: `column`,
                height: "auto",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  width: "100%",
                  marginTop: "30px",
                  height: "auto",
                }}
              >
                <SearchBar2
                  width="80%"
                  getSearchDetails={getSearchDetails}
                  userMode={true}
                />
              </Box>
              <Box className="boxContent" sx={{ justifyContent: "center" }}>
                {copyUsers &&
                  usersToDisplay.map((user, index) => {
                    return (
                      <Box key={index} sx={{ mr: 10, mt: 5 }}>
                        <Users
                          user={user}
                          handleGetAllUsers={handleGetAllUsers}
                        />
                      </Box>
                    );
                  })}
              </Box>
              <Pagination
                count={Math.ceil(copyUsers.length / postsPerPage)}
                page={currentPageUser}
                onChange={handlePageChangeUser}
                color="primary"
                sx={{ display: "flex", justifyContent: "center" }}
              />
            </Box>
          </>
        )}
      </Paper>
    )
  );
};

export default ProfileSettingsView;
