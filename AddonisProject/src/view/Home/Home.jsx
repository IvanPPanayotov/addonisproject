import { useState, useEffect } from "react";
import { getAllAddonsFromDatabase } from "../../services/addons.services";
import { Box } from "@mui/material";
import {
  getNewestAddons,
  getFeaturedAddons,
  getMostDownloadedAddons,
} from "../../services/addons.services";
import Slider from "../../components/AddonContainer/splider";
import SearchBar2 from "../../components/SearchBar/SearchBar2";

const Home = () => {
  const [addons, setAddons] = useState([]);
  const [mostRecentAddons, setMostRecentAddons] = useState([]);
  const [featuredAddons, setFeaturedAddons] = useState([]);
  const [mostDownloadedAddons, setMostDownloadedAddons] = useState([]);

  const handleGetAddons = async () => {
    const addons = await getAllAddonsFromDatabase();
    const mostRecent = await getNewestAddons();
    const featured = await getFeaturedAddons();
    const mostDownloaded = await getMostDownloadedAddons();
    setAddons(addons);
    setMostRecentAddons(mostRecent);
    setFeaturedAddons(featured);
    setMostDownloadedAddons(mostDownloaded);
  };

  useEffect(() => {}, [addons]);

  useEffect(() => {
    handleGetAddons();
  }, []);

  return (
    <Box sx={{ width: `100%` }}>
      <Box sx={{ mt: 3 }}>
        <Box
          sx={{
            display: `flex`,
            alignItems: `center`,
            justifyContent: `center`,
            flexDirection: `column`,
          }}
        >
          <SearchBar2 />
        </Box>

        <>
          <Slider title={"Featured"} addons={featuredAddons} />
          <Slider title={"Popular"} addons={mostDownloadedAddons} />
          <Slider title={"New"} addons={mostRecentAddons} />
        </>
      </Box>
    </Box>
  );
};

export default Home;
