import { useRef, useState } from "react";
import emailjs from "@emailjs/browser";
import { Paper, Box, TextField, Button, Container } from "@mui/material";
import { useUniqueValues } from "../../context/UniqueValuesContext";
import AlertSuccess from "../../components/Alert/AlertSuccess/AlertSuccess";

const InviteFriendComponent = () => {
  const { uniqueEmails } = useUniqueValues();
  const formRef = useRef();
  const [success, setSuccess] = useState(false);

  const sendEmail = (e) => {
    e.preventDefault();
    const form = formRef.current;
    const friendEmail = form.querySelector('input[name="user_email"]').value;
    if (!uniqueEmails.has(friendEmail)) {
      emailjs
        .sendForm(
          "service_pru43pm",
          "template_d0lx2d6",
          formRef.current,
          "6US69Wjr3IOESdBQ8"
        )
        .then(
          (result) => {
            console.log(result.text);
            setSuccess(true);
          },
          (error) => {
            console.log(error.text);
          }
        );
    } else {
      alert("This person is already registered!");
    }
  };

  return (
    <Box
      className="boxContent"
      sx={{
        display: `flex`,
        alignItems: `center`,
        justifyContent: `center`,
        flexDirection: `column`,
        height: "auto",
      }}
    >
      <Paper
        sx={{
          width: "40vw",
          minWidth: "200px",
          borderRadius: "30px",
          p: 3,
          textAlign: "center",
          boxShadow: "0px 0px 10px rgba(105, 255, 253, 0.70)",
        }}
      >
        {success && <AlertSuccess message={"Invitation request "} />}
        <Container component="form" ref={formRef} onSubmit={sendEmail}>
          <TextField
            label="Friend Email"
            name="user_email"
            type="email"
            variant="outlined"
            fullWidth
            margin="normal"
          />
          <Button type="submit" variant="contained" color="primary">
            Send
          </Button>
        </Container>
      </Paper>
    </Box>
  );
};

export default InviteFriendComponent;
