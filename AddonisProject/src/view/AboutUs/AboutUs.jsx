import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import yasen from "../../resources/yasen1.jpg";
import dimitar from "../../resources/dimitar.jpg";
import ivan from "../../resources/Ivan.P.jpg";

import GitHubIcon from "@mui/icons-material/GitHub";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import { useTheme } from "../../context/ThemeContext";
import { useEffect, useState } from "react";

const AboutUs = () => {
  const [colorIcon, setColorIcon] = useState();
  const { mode } = useTheme();

  useEffect(() => {
    if (mode == false) {
      setColorIcon(`black`);
    } else setColorIcon(`white`);
  }, [mode]);

  const teamMembers = [
    {
      id: 1,
      firstName: "Ivan",
      lastName: "Panayotov",
      description: "Telerik Academy Graduate",
      avatarUrl: ivan,
      gitLabLink: "https://gitlab.com/IvanPPanayotov/",
      linkedInLink: "https://www.linkedin.com/in/ivan-panayotov/",
    },
    {
      id: 2,
      firstName: "Dimitar",
      lastName: "Dimitrov",
      description: "Telerik Academy Graduate",
      avatarUrl: dimitar,
      gitLabLink: "https://gitlab.com/dimitarpenchevdimitrov92",
      linkedInLink: "https://www.linkedin.com/in/dimitardimitrov92/",
    },
    {
      id: 3,
      firstName: "Yasen",
      lastName: "Stefanov",
      description: "Telerik Academy Graduate",
      avatarUrl: yasen,
      gitLabLink: "https://gitlab.com/Yasen.Stefanov.a49",
      linkedInLink: "https://www.linkedin.com/in/yasen-stefanov-797749207/",
    },
  ];

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        gap: "15vw",
        height: "100vh",
      }}
    >
      {teamMembers.map((member) => (
        <div
          key={member.id}
          style={{ textAlign: "center", margin: "0 20px 150px" }}
        >
          <Avatar
            alt={`${member.firstName} ${member.lastName}`}
            src={member.avatarUrl}
            sx={{
              width: 165,
              height: 165,
              margin: "0 auto",
              marginBottom: "10px",
              boxShadow: "0px 0px 120px 10px rgba(105, 255, 253, 0.4)",
            }}
          />
          <Typography color="text.primary" variant="h6" mt={5}>
            {member.firstName} {member.lastName}
          </Typography>
          <Typography
            mt={1}
            color="text.primary"
            variant="body2"
            marginBottom={"10px"}
          >
            {member.description}
          </Typography>
          <div>
            <a
              href={member.gitLabLink}
              target="_blank"
              rel="noopener noreferrer"
            >
              <GitHubIcon style={{ color: `${colorIcon}` }} />
            </a>
            <a
              href={member.linkedInLink}
              target="_blank"
              rel="noopener noreferrer"
            >
              <LinkedInIcon
                style={{ color: `${colorIcon}`, marginLeft: "10px" }}
              />
            </a>
          </div>
        </div>
      ))}
    </div>
  );
};

export default AboutUs;
