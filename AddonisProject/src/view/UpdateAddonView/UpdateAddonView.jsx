import { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import { ref, update } from "firebase/database";
import { ref as sRef } from "firebase/storage";
import db, { storage } from "../../config/fireBase.config";
import {
  Button,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  FormHelperText,
  Avatar,
} from "@mui/material";
import CloudUploadIcon from "@mui/icons-material/CloudUpload";
import {
  validateAddonName,
  validateAddonDescription,
  validateTags,
  isValidURL,
} from "../../services/validations";
import { useLocation } from "react-router-dom";
import { getDownloadURL, uploadBytes } from "firebase/storage";
import { useUniqueValues } from "../../context/UniqueValuesContext";
import { useAuth } from "../../context/AuthenticationContext";
import useNavigation from "../../hooks/Navigate";
import AlertInfoSnackbar from "../../components/Alert/AlertInfoSnackbar/AlertInfoSnackbar";

export default function UpdateAddonView() {
  const [addon, setAddon] = useState(null);
  const [addonName, setAddonName] = useState("");
  const [addonPicture, setAddonPicture] = useState(null);
  const [addonDescription, setAddonDescription] = useState("");
  const [tags, setTags] = useState([]);
  const [IDEName, setIDEName] = useState("");
  const [originLink, setOriginLink] = useState("");
  const [uploadedPicture, setUploadedPicture] = useState(null);

  const [addonNameValid, setAddonNameValid] = useState(true);
  const [addonDescriptionValid, setAddonDescriptionValid] = useState(true);
  const [addonTagsValid, setAddonTagsValid] = useState(true);
  const [error, setError] = useState(false);
  const [alertInfo, setAlertInfo] = useState(false);
  const [originLinkValid, setOriginLinkValid] = useState(true);
  const isIDENameValid = IDEName !== "";

  const location = useLocation();
  const { handleNavigation } = useNavigation();

  const { uniqueAddonName } = useUniqueValues();
  const { currentUserData } = useAuth();

  useEffect(() => {
    const addon = location.state;
    if (addon) {
      setAddon(addon);
      setAddonName(addon.addon.addonName);
      setAddonDescription(addon.addon.description);
      setTags(addon.addon.tags);
      setIDEName(addon.addon.IDEName);
      setOriginLink(addon.addon.originLink ? addon.addon.originLink : "");
      setAddonPicture(addon.addon.addonPicture);
    }
  }, [currentUserData]);

  const handleUpdateButton = async () => {
    let isAllValid;
    if (!originLink) {
      isAllValid =
        validateAddonDescription(addonDescription) &&
        validateTags(tags) &&
        isIDENameValid;
    } else {
      isAllValid =
        validateAddonDescription(addonDescription) &&
        validateTags(tags) &&
        isValidURL(originLink) &&
        isIDENameValid;
    }

    if (!isAllValid) {
      if (!addonName === addon.addon.addonName) {
        setAddonNameValid(
          validateAddonName(addonName) && !uniqueAddonName.has(addonName)
        );
      } else setAddonNameValid(true);

      setAddonDescriptionValid(validateAddonDescription(addonDescription));
      setAddonTagsValid(validateTags(tags));
      if (originLink != `No origin link`) {
        setOriginLinkValid(isValidURL(originLink));
      } else await updateAddonService(addon.addon.addonID);

      setError(true);
    } else {
      await updateAddonService(addon.addon.addonID);
    }
  };

  const updateAddonService = async (uid) => {
    const downloadURL = await storageUpload(uploadedPicture);

    const addonRef = ref(db, `addons/${uid}`);

    const updatedAddon = {
      addonName: addonName,
      description: addonDescription,
      IDEName: IDEName,
      originLink: originLink,
      addonPicture: uploadedPicture ? downloadURL : addon.addon.addonPicture,
      tags: tags,
      status: "pending",
    };

    try {
      await update(addonRef, updatedAddon);
      handleNavigation(`/`);
    } catch (error) {
      console.error("Error updating addon:", error);
    }
  };

  const storageUpload = async (uploadedPicture) => {
    if (uploadedPicture) {
      const storageRef = sRef(storage, `${uploadedPicture.name}`);
      await uploadBytes(storageRef, uploadedPicture);
      const downloadURL = await getDownloadURL(storageRef);
      return downloadURL;
    }
    return null;
  };

  const handleTagInput = (event) => {
    const inputTags = event.target.value
      .split("#")
      .map((tag) => tag.trim())
      .filter((tag) => tag !== "");

    setTags(inputTags);
  };

  const handlePictureChange = (e) => {
    const file = e.target.files[0];

    if (file) {
      if (file.type.startsWith("image/") || file.name.endsWith(".gif")) {
        setUploadedPicture(file);
      } else {
        setAlertInfo(true);
      }
    }
  };

  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        minHeight: "100vh",
      }}
    >
      <Box
        sx={{
          width: "50%",
          borderRadius: "30px",
          boxShadow: "0px 0px 10px rgba(105, 255, 253, 0.70)",
          p: 3,
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Avatar
            src={addonPicture}
            style={{ width: "120px", height: "120px", marginBottom: "30px" }}
          />
        </Box>

        {addonNameValid ? (
          <TextField
            label="Name"
            value={addonName}
            variant="outlined"
            fullWidth
            sx={{ borderRadius: "30px", marginBottom: "1rem" }}
            onChange={(event) => setAddonName(event.target.value)}
          />
        ) : (
          <TextField
            fullWidth
            error
            margin="normal"
            id="addonName"
            label={
              uniqueAddonName.has(addonName)
                ? "Name exist"
                : "Between 2-20 symbols"
            }
            value={""}
            onFocus={() => {
              setAddonNameValid(true);
              setError(false);
            }}
          />
        )}

        {addonDescriptionValid ? (
          <TextField
            value={addonDescription}
            label="Description"
            variant="outlined"
            fullWidth
            sx={{ borderRadius: "30px", marginBottom: "1rem" }}
            onChange={(event) => setAddonDescription(event.target.value)}
          />
        ) : (
          <TextField
            fullWidth
            error
            margin="normal"
            id="addonDescription"
            label={"Between 16-64 symbols"}
            value={""}
            onFocus={() => {
              setAddonDescriptionValid(true);
              setError(false);
            }}
          />
        )}

        <FormControl
          sx={{ marginBottom: "1rem" }}
          variant="outlined"
          fullWidth
          required
          error={!isIDENameValid}
        >
          <InputLabel>IDE Name</InputLabel>
          <Select
            value={IDEName}
            onChange={(event) => setIDEName(event.target.value)}
            label=""
          >
            <MenuItem value="">
              <em>Select IDE</em>
            </MenuItem>
            <MenuItem value="Visual Studio Code">Visual Studio Code</MenuItem>
            <MenuItem value="WebStorm">WebStorm</MenuItem>
            <MenuItem value="Sublime Text">Sublime Text</MenuItem>
          </Select>
          {!isIDENameValid && (
            <FormHelperText>Please select an IDE.</FormHelperText>
          )}
        </FormControl>

        {originLink && originLink !== `No origin link` && (
          <>
            {originLinkValid ? (
              <TextField
                value={originLink}
                label="Origin Link"
                variant="outlined"
                fullWidth
                sx={{ borderRadius: "30px", marginBottom: "1rem" }}
                onChange={(event) => setOriginLink(event.target.value)}
              />
            ) : (
              <TextField
                fullWidth
                error
                margin="normal"
                id="originLink"
                label={"Enter a valid link"}
                value={""}
                onFocus={() => {
                  setOriginLinkValid(true);
                  setError(false);
                }}
              />
            )}
          </>
        )}

        {addonTagsValid ? (
          <TextField
            label="Tags"
            variant="outlined"
            fullWidth
            sx={{ borderRadius: "30px", marginBottom: "1rem" }}
            onChange={handleTagInput}
          />
        ) : (
          <TextField
            fullWidth
            error
            margin="normal"
            id="originLink"
            label={"You must add a tag!"}
            value={""}
            onFocus={() => {
              setAddonTagsValid(true);
              setError(false);
            }}
          />
        )}

        {alertInfo && (
          <AlertInfoSnackbar message={"Please upload a valid image file"} />
        )}
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            marginBottom: "1rem",
          }}
        >
          <label htmlFor="picture-input" style={{ cursor: "pointer" }}>
            <input
              id="picture-input"
              type="file"
              onChange={handlePictureChange}
              accept=".jpg, .jpeg, .png, .pdf"
              style={{ display: "none" }}
            />
            <Button
              onClick={() => setAlertInfo(false)}
              variant="contained"
              color="secondary"
              component="span"
              startIcon={<CloudUploadIcon />}
            >
              Upload Picture
            </Button>
          </label>
        </Box>
        <Button variant="contained" onClick={handleUpdateButton} fullWidth>
          Update
        </Button>
      </Box>
    </Box>
  );
}
