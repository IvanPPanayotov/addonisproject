import {
  Box,
  FormControl,
  FormControlLabel,
  FormLabel,
  Pagination,
  Radio,
  RadioGroup,
} from "@mui/material";
import { useEffect } from "react";
import SearchBar2 from "../../components/SearchBar/SearchBar2";
import { getAddonByKey, getAllPostsWithIDE, sortBy } from "../../services/addons.services";
import { useState } from "react";
import AddonItem from "../../components/AddonItem/AddonItem";
import { getAllApprovedFromDatabase } from "../../services/users.services";
import { useLocation, useParams } from "react-router-dom";
import { onSearch } from "../../services/search.service";
import TagList from "../../components/TagList/TagList";
import { getAllPostsFromTag } from "../../services/tags.services";
import AuthorList from "../../components/AuthorList/AuthorList";
import IdeList from "../../components/IdeList/IdeList";

export default function SearchContentView() {
  const [value, setValue] = useState("");
  const [allAddons, setAllAddons] = useState();
  const [copyAddons, setCopyAddons] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const postsPerPage = 10;

  const { searchContent, searchBy } = useParams();

  const [render, setRender] = useState(false);

  const location = useLocation();

  const startIndex = (currentPage - 1) * postsPerPage;
  const endIndex = Math.min(startIndex + postsPerPage, copyAddons.length);
  const postsToDisplay = copyAddons.slice(startIndex, endIndex);

  useEffect(() => {
    const getAddons = async () => {
      const allAddons = await getAllApprovedFromDatabase();
      if (searchContent) {
        const searchedAddons = await onSearch(allAddons, searchContent, searchBy);

        setAllAddons(searchedAddons);
        setCopyAddons(searchedAddons);
        return;
      }

      setAllAddons(allAddons);
      setCopyAddons(allAddons);
    };
    getAddons();
  }, [location]);

  useEffect(() => {}, [render]);

  const handleChange = (event) => {
    setValue(event.target.value);
    setRender(!render);
  };
  const handlePageChange = (event, newPage) => {
    setCurrentPage(newPage);
  };

  const handleSort = async (sort) => {
    const sortedArr = await sortBy(sort, copyAddons);
    setCopyAddons(sortedArr);
  };

  const changeTag = async (tag) => {
    console.log(tag);
    if (!tag) {
      setCopyAddons(allAddons);
      return;
    }
    setCopyAddons(await getAllPostsFromTag(tag));
  };

  const changeIDE = async (IDE) => {
    if (!IDE) {
      setCopyAddons(allAddons);
      return;
    }
    setCopyAddons(await getAllPostsWithIDE(allAddons, IDE));
  };

  const changeAuthor = async (author) => {
    if (!author) {
      setCopyAddons(allAddons);
      return;
    }
    if (author.length === 0) {
      setCopyAddons([]);
      return;
    }
    const authorAddons = await Promise.all(
      author.map(async (addonID) => {
        const addonVal = await getAddonByKey(addonID);
        return addonVal;
      })
    );

    setCopyAddons(authorAddons);
  };

  return (
    <Box
      className={`container`}
      sx={{
        pt: 2,
        pb: 2,
        width: `100%`,
        height: `100%`,

        display: `flex`,
        justifyContent: `center`,
        alignItems: `center`,
        flexDirection: `column`,
      }}>
      <SearchBar2 />
      <Box
        className={`content-container`}
        sx={{
          mt: 10,
          mb: 5,
          display: `flex`,
          width: `100%`,
        }}>
        <Box
          className={`options-container`}
          sx={{ display: `flex`, flexDirection: `column`, ml: 3 }}>
          <FormControl>
            <FormLabel>Sort By</FormLabel>
            <RadioGroup
              value={value}
              onChange={handleChange}
              sx={{ width: `170px`, color: `text.primary` }}>
              <FormControlLabel
                value="Name"
                control={<Radio />}
                onClick={() => {
                  handleSort(`Name`);
                }}
                label="Name"
              />
              <FormControlLabel
                value="Tags"
                onClick={() => {
                  handleSort(`Tags`);
                }}
                control={<Radio />}
                label="Tags"
              />
              <FormControlLabel
                value="Downloads"
                onClick={() => {
                  handleSort(`Downloads`);
                }}
                control={<Radio />}
                label="Downloads"
              />
              <FormControlLabel
                value="Date"
                onClick={() => {
                  handleSort(`Upload Date`);
                }}
                control={<Radio />}
                label="Upload Date"
              />
            </RadioGroup>
          </FormControl>
          <FormControl>
            <FormLabel sx={{ mb: 2 }}>Filter</FormLabel>
            <TagList changeTag={changeTag} />
            <Box sx={{ mt: 2 }}>
              <AuthorList changeAuthor={changeAuthor} />
            </Box>
            <Box sx={{ mt: 2 }}>
              <IdeList changeIDE={changeIDE} />
            </Box>
          </FormControl>
        </Box>
        <Box
          className={`addons-container`}
          sx={{
            width: `100%`,
            display: `flex`,
            flexDirection: `column`,

            ml: 10,
          }}>
          <Pagination
            count={Math.ceil(copyAddons.length / postsPerPage)}
            page={currentPage}
            onChange={handlePageChange}
            color="primary"
            sx={{ display: "flex", justifyContent: "center" }}
          />

          <Box sx={{ display: `flex`, flexWrap: `wrap` }}>
            {copyAddons &&
              postsToDisplay.map((addon, index) => {
                return (
                  <Box key={index} sx={{ mr: 10, mt: 5 }}>
                    <AddonItem addon={addon} addons={copyAddons} />
                  </Box>
                );
              })}
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
