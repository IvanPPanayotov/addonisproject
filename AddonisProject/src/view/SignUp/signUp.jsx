import { useState, useEffect } from "react";
import { Backdrop, Button, Fade, Modal, Paper, TextField } from "@mui/material";
import {
  validateUserName,
  validatePhone,
  validateEmail,
  validatePassword,
} from "../../services/validations";
import AlertInfoSnackbar from "../../components/Alert/AlertInfoSnackbar/AlertInfoSnackbar";

import InputAdornment from "@mui/material/InputAdornment";
import PhoneIcon from "@mui/icons-material/Phone";
import { MuiTelInput } from "mui-tel-input";

import { useAuth } from "../../context/AuthenticationContext";
import { useModal } from "../../context/Open&CloseModals";
import { useUniqueValues } from "../../context/UniqueValuesContext";
import AlertSuccess from "../../components/Alert/AlertSuccess/AlertSuccess";

const Signup = () => {
  const [userName, setUserName] = useState("");
  const [userNameValid, setUserNameValid] = useState(true);

  const [phone, setPhone] = useState("");
  const [phoneValid, setPhoneValid] = useState(true);

  const [email, setEmail] = useState("");
  const [emailValid, setEmailValid] = useState(true);

  const [password, setPassword] = useState("");
  const [isPasswordValid, setIsPasswordValid] = useState(true);

  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);

  const { signUp } = useAuth();
  const { uniqueUsers, uniquePhones, uniqueEmails } = useUniqueValues();

  const { signInModal, closeSignInModal, openSignInModal } = useModal();

  useEffect(() => {
    openSignInModal();
  }, []);

  const handleSubmit = (event) => {
    setError(false);
    event.preventDefault();

    const isAllValid =
      !uniqueUsers.has(userName) &&
      !uniqueEmails.has(email) &&
      !uniquePhones.has(phone) &&
      validateUserName(userName) &&
      validateEmail(email) &&
      validatePassword(password) &&
      validatePhone(phone);
    if (!isAllValid) {
      setUserNameValid(
        validateUserName(userName) && !uniqueUsers.has(userName)
      );
      setEmailValid(validateEmail(email) && !uniqueEmails.has(email));
      setPhoneValid(validatePhone(phone) && !uniquePhones.has(phone));
      setIsPasswordValid(validatePassword(password));
      setError(true);
    } else {
      closeSignInModal();
      signUp(email, password, userName, phone);
      resetTextFields();
      setSuccess(true);
    }
  };

  const resetTextFields = () => {
    setUserName(``);
    setEmail(``);
    setPassword(``);
    setPhone(``);
  };

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 600,
    color: `black`,
    borderRadius: 5,

    border: "none",
    radius: "4px",
    boxShadow: 24,
    p: 4,
  };

  return (
    <div>
      {success && (
        <>
          <AlertInfoSnackbar
            message={
              "We have sent an activation message to the above email. Please confirm it."
            }
          />{" "}
          <AlertSuccess />
        </>
      )}
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={signInModal}
        onClose={() => {
          closeSignInModal();
          resetTextFields();
        }}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={signInModal}>
          <Paper sx={style}>
            <form onSubmit={handleSubmit}>
              <div>
                {userNameValid ? (
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="userName"
                    label="Username"
                    value={userName}
                    onChange={(event) => {
                      setUserName(event.target.value);
                      setUserNameValid(true);
                      setError(false);
                    }}
                  />
                ) : (
                  <TextField
                    fullWidth
                    error
                    margin="normal"
                    id="userName"
                    label={
                      uniqueUsers.has(userName)
                        ? "Name exist"
                        : "Between 2-20 symbols"
                    }
                    value={""}
                    onFocus={() => {
                      setUserNameValid(true);
                      setError(false);
                    }}
                  />
                )}

                {phoneValid ? (
                  <MuiTelInput
                    required
                    margin="normal"
                    sx={{ width: "600px", marginBottom: "1rem" }}
                    value={phone}
                    label="Phone"
                    defaultCountry="BG"
                    variant="outlined"
                    onChange={(val) => {
                      setPhone(val.replace(/ /g, ""));
                      setPhoneValid(true);
                      setError(false);
                    }}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <PhoneIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                ) : (
                  <MuiTelInput
                    sx={{ width: "600px", marginBottom: "1rem" }}
                    margin="normal"
                    value={""}
                    error
                    defaultCountry="BG"
                    label={
                      uniquePhones.has(phone) ? "Phone exist" : "Invalid phone"
                    }
                    variant="outlined"
                    onFocus={() => {
                      setPhoneValid(true);
                      setError(false);
                    }}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <PhoneIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                )}

                {emailValid ? (
                  <TextField
                    margin="normal"
                    required
                    id="email"
                    label="Email "
                    fullWidth
                    value={email}
                    onChange={(event) => {
                      setEmail(event.target.value);
                      setEmailValid(true);
                      setError(false);
                    }}
                  />
                ) : (
                  <TextField
                    error
                    fullWidth
                    margin="normal"
                    id="email"
                    label={
                      uniqueEmails.has(email) ? "Email exist" : "Invalid email"
                    }
                    value={""}
                    onFocus={() => {
                      setEmailValid(true);
                      setError(false);
                    }}
                  />
                )}

                {isPasswordValid ? (
                  <TextField
                    margin="normal"
                    required
                    id="password"
                    label="Password"
                    type="password"
                    autoComplete="off"
                    fullWidth
                    value={password}
                    onChange={(event) => {
                      setPassword(event.target.value);
                      setIsPasswordValid(true);
                      setError(false);
                    }}
                  />
                ) : (
                  <TextField
                    error
                    fullWidth
                    margin="normal"
                    id="password"
                    type="password"
                    label="Invalid password"
                    autoComplete="off"
                    value={""}
                    onFocus={() => {
                      setIsPasswordValid(true);
                      setError(false);
                    }}
                  />
                )}
              </div>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Sign In
              </Button>
            </form>
          </Paper>
        </Fade>
      </Modal>
    </div>
  );
};

export default Signup;
