import { Avatar, Box, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import DownloadSharpIcon from "@mui/icons-material/DownloadSharp";
import PriorityHighRoundedIcon from "@mui/icons-material/PriorityHighRounded";
import GradeRoundedIcon from "@mui/icons-material/GradeRounded";
import OverViewAddon from "../../components/OverViewAddon/OverViewAddon";
import ReviewOfAddon from "../../components/ReviewsOfAddon/ReviewOfAddon";
import { useLocation } from "react-router";
import { averageRating } from "../../services/rateAddon.service";
import { getAddonDownloadCount } from "../../services/addons.services";
import EditPost from "../../components/EdditBtn/EdditBtn";
import { getIssues } from "../../services/users.services";

export default function SingleAddonView() {
  const [activeTab, setActiveTab] = useState("Overview");
  const [rating, setRating] = useState(0);
  const [downloadCount, setDownloadCount] = useState(0);
  const [issuesCount, setIssuesCount] = useState();
  const location = useLocation();
  const addon = location.state;

  useEffect(() => {
    const addonData = async () => {
      const rate = await averageRating(addon.addonID);
      const count = await getAddonDownloadCount(addon.addonID);
      const issues = await getIssues([addon]);
      setIssuesCount(issues);
      setDownloadCount(count);
      setRating(rate);
    };
    addonData();
  }, []);

  const handleTabClick = (tab) => {
    setActiveTab(tab);
  };
  return (
    <Box
      sx={{
        pb: 50,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Box
        className={`addon-info`}
        sx={{
          position: `absolute`,
          top: 64,
          left: 0,
          width: `100%`,
          height: `370px`,
          backgroundColor: "transparent",
          backgroundImage: `url(${addon.addonPicture})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          display: `flex`,
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            position: "absolute",
            width: "100%",
            height: "100%",
            backgroundColor: "rgba(0, 0, 0, 0.3)",
            backdropFilter: "blur(30px)",
          }}
        />

        <Avatar
          src={addon.addonPicture}
          sx={{ bottom: 50, width: 250, height: 250 }}
        ></Avatar>
        {}
        <Box sx={{ position: `absolute`, right: 50, top: 30 }}>
          <EditPost addon={addon}></EditPost>
        </Box>
        <Box sx={{ bottom: 60, position: `absolute` }}>
          <Typography
            variant="h4"
            sx={{
              fontWeight: "bold",
              bgcolor: "rgba(0, 0, 0, 0.3)",
              color: "white",
              borderRadius: 20,
              p: `3px`,
              cursor: `default`,
            }}
          >
            {addon.addonName}
          </Typography>
        </Box>
        <Box
          sx={{
            position: `absolute`,
            top: `87%`,
            width: "100%",
            maxWidth: `100%`,
            display: "flex",
            justifyContent: "space-evenly",
          }}
        >
          <Box sx={{ display: "flex", alignItems: "start" }}>
            <Typography
              sx={{
                color: "white",
                bgcolor: "rgba(0, 0, 0, 0.3)",
                borderRadius: 20,
                p: `3px`,
              }}
            >
              Users rating: {rating}/5
              <GradeRoundedIcon />
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <Typography
              sx={{
                bgcolor: "rgba(0, 0, 0, 0.3)",
                color: "white",
                borderRadius: 20,
                p: `3px`,
              }}
            >
              <DownloadSharpIcon />
              Downloads: {downloadCount}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <Typography
              sx={{
                bgcolor: "rgba(0, 0, 0, 0.3)",
                color: "white",
                borderRadius: 20,
                p: `3px`,
              }}
            >
              <PriorityHighRoundedIcon />
              Open Issues: {issuesCount}
            </Typography>
          </Box>
        </Box>
      </Box>
      <Box
        className={`details-selection`}
        sx={{
          width: "90vw",
          maxWidth: "90vw",
          display: "flex",
          justifyContent: "flex-start",
          flexDirection: "column",
          position: "relative",
          top: "370px",
        }}
      >
        <Box display={`flex`}>
          <Typography
            onClick={() => handleTabClick("Overview")}
            sx={{
              marginRight: "40px",
              borderBottom: `2px solid ${
                activeTab === "Overview" ? "#3498db" : "inherit"
              }`,

              ml: 10,
              fontSize: "1rem",
              cursor: "pointer",
              color: activeTab === "Overview" ? "#3498db" : "text.primary",
            }}
          >
            Overview
          </Typography>
          <Typography
            onClick={() => handleTabClick("Reviews")}
            sx={{
              fontSize: "1rem",
              borderBottom: `2px solid ${
                activeTab === "Reviews" ? "#3498db" : "inherit"
              }`,

              cursor: "pointer",
              color: activeTab === "Reviews" ? "#3498db" : "text.primary",
            }}
          >
            Reviews
          </Typography>
        </Box>
        {activeTab === `Overview` ? (
          <OverViewAddon addon={addon} />
        ) : (
          <ReviewOfAddon addon={addon} />
        )}
      </Box>
    </Box>
  );
}
