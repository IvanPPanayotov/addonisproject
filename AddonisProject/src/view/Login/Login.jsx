import { useState } from "react";
import AlertError from "../../components/Alert/AlertError/AlertError";
import AlertSuccess from "../../components/Alert/AlertSuccess/AlertSuccess";
import {
  Backdrop,
  Button,
  Fade,
  Grid,
  Modal,
  Paper,
  TextField,
} from "@mui/material";

import { useAuth } from "../../context/AuthenticationContext";
import { useModal } from "../../context/Open&CloseModals";
import Signup from "../SignUp/signUp";

const Login = () => {
  const [email, setEmail] = useState("");
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [password, setPassword] = useState("");
  const [isSignUpVisible, setIsSignUpVisible] = useState(false);

  const { logIn } = useAuth();

  const { logInModal, closeLogInModal, openSignInModal } = useModal();

  const handleSubmitButton = async (event) => {
    event.preventDefault();
    setError(false);
    setSuccess(false);

    try {
      await logIn(email, password);
      setPassword("");
      closeLogInModal();
      setSuccess(true);
    } catch (error) {
      setError(true);
      console.log(`Fail to Log in: ` + error);
    }
  };

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 500,
    color: `black`,
    border: "none",
    borderRadius: 5,
    radius: "15px",
    boxShadow: 24,
    p: 4,
  };

  return (
    <div>
      {error && <AlertError message="Login" />}
      {success && <AlertSuccess message="Login" />}
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={logInModal}
        onClose={() => {
          closeLogInModal();
          setPassword(``);
          setEmail(``);
        }}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={logInModal}>
          <Paper sx={style}>
            <form className="login-form" onSubmit={handleSubmitButton}>
              <div className="form-field">
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  onChange={(event) => setEmail(event.target.value)}
                />
              </div>
              <div className="form-field">
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="off"
                  value={password}
                  onChange={(event) => setPassword(event.target.value)}
                />
              </div>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Sign In
              </Button>
              <Grid container>
                <Grid item>
                  {`  `}
                  <a
                    href="#"
                    onClick={() => {
                      closeLogInModal(),
                        setIsSignUpVisible(true),
                        setPassword(``),
                        setEmail(``),
                        openSignInModal();
                    }}
                  >
                    {"Don't have an account? Sign Up"}
                  </a>
                </Grid>
              </Grid>
            </form>
          </Paper>
        </Fade>
      </Modal>
      {isSignUpVisible && <Signup />}
    </div>
  );
};

export default Login;
