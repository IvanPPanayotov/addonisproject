import { Octokit } from "octokit";
export const EMAIL_REGEX = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

export const PASSWORD_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%]).{8,24}$/;

export const owner = `Ivan-P-P99`;
export const repository = `StorageAddonis`;

export const octokit = new Octokit({
    auth: "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",
});

export const gitHubRegex = /^(https?:\/\/)?(www\.)?(github\.com)(:[0-9]+)?(\/[^\s]*)?$/;