import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";
import { getAuth } from "firebase/auth";
import { getStorage } from "firebase/storage";

import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyC1WsCnulCGLcHIeZSVErUHjooH77iJuMc",
  authDomain: "addonis-final.firebaseapp.com",
  databaseURL: "https://addonis-final-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "addonis-final",
  storageBucket: "addonis-final.appspot.com",
  messagingSenderId: "425719157329",
  appId: "1:425719157329:web:817153346066516f1a5e70",
};
export const app = initializeApp(firebaseConfig);
export const storage = getStorage(app);
export const auth = getAuth(app);
const db = getDatabase(app);
export default db;
